-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 19, 2022 at 01:53 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_tracing`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang_logistik`
--

CREATE TABLE `tbl_barang_logistik` (
  `barang_no` int(11) NOT NULL,
  `barang_resi` varchar(30) NOT NULL,
  `barang_kode` varchar(30) DEFAULT NULL,
  `barang_nama` varchar(30) NOT NULL,
  `barang_hg_satuan` int(11) DEFAULT NULL,
  `barang_qty` int(11) NOT NULL,
  `barang_satuan` varchar(30) NOT NULL,
  `barang_hg_total` int(11) DEFAULT NULL,
  `barang_ket` text NOT NULL,
  `barang_stts` varchar(30) NOT NULL COMMENT 'GAk DIPAKE DULU\r\n',
  `barang_created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_barang_logistik`
--

INSERT INTO `tbl_barang_logistik` (`barang_no`, `barang_resi`, `barang_kode`, `barang_nama`, `barang_hg_satuan`, `barang_qty`, `barang_satuan`, `barang_hg_total`, `barang_ket`, `barang_stts`, `barang_created_at`) VALUES
(27, 'S218729131213', NULL, 'TES', NULL, 1, 'Unit', 1000000, '', '', '2022-02-09 21:38:50'),
(28, 'S218729131213', NULL, 'Meja tulis guru sekolah', NULL, 2, 'Unit', 2000000, '', '', '2022-02-09 21:43:29'),
(29, 'S2183434312342', NULL, 'Meja', NULL, 2, 'Unit', 400000, '', '', '2022-02-19 18:07:10'),
(31, 'S2183434312342', NULL, 'Kursi', NULL, 1, 'Unit', 200000, '', '', '2022-02-19 18:09:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_config`
--

CREATE TABLE `tbl_config` (
  `id_config` int(1) NOT NULL,
  `versi` varchar(50) NOT NULL,
  `nama_app` varchar(50) NOT NULL,
  `icon_app` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_config`
--

INSERT INTO `tbl_config` (`id_config`, `versi`, `nama_app`, `icon_app`) VALUES
(1, 'LOGISTICS TRACING SYSTEM ', 'LOGISTICS <b>TRACING</b> SYSTEM ', 'checklist.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_resi`
--

CREATE TABLE `tbl_resi` (
  `resi_no` int(11) NOT NULL,
  `resi_internal` varchar(30) NOT NULL COMMENT 'Resi Internal',
  `resi_kode` varchar(30) NOT NULL,
  `resi_tujuan` varchar(50) NOT NULL,
  `resi_created_at` datetime NOT NULL,
  `resi_updated_at` datetime NOT NULL COMMENT 'UPDATE KETIKA SELESAI',
  `resi_stts` varchar(30) NOT NULL COMMENT 'Prosess, Sukses, Batal',
  `resi_bast` varchar(50) NOT NULL,
  `resi_kendaraan` varchar(30) NOT NULL,
  `resi_no_polisi` varchar(10) NOT NULL,
  `resi_lokasi_muat` varchar(30) NOT NULL,
  `resi_alamat_muat1` varchar(50) NOT NULL,
  `resi_alamat_muat2` varchar(50) NOT NULL,
  `resi_lokasi_bongkar` varchar(30) NOT NULL,
  `resi_alamat_bongkar1` varchar(50) NOT NULL,
  `resi_alamat_bongkar2` varchar(50) NOT NULL,
  `resi_ket` text NOT NULL,
  `resi_qrcode` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_resi`
--

INSERT INTO `tbl_resi` (`resi_no`, `resi_internal`, `resi_kode`, `resi_tujuan`, `resi_created_at`, `resi_updated_at`, `resi_stts`, `resi_bast`, `resi_kendaraan`, `resi_no_polisi`, `resi_lokasi_muat`, `resi_alamat_muat1`, `resi_alamat_muat2`, `resi_lokasi_bongkar`, `resi_alamat_bongkar1`, `resi_alamat_bongkar2`, `resi_ket`, `resi_qrcode`) VALUES
(24, 'GP203902112', 'S218233312342', 'Kota Bandung', '2022-02-07 09:51:44', '2022-02-07 03:53:20', 'Selesai', 'S218233312342.pdf', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', 'TES', 'S218233312342.png'),
(25, '-', 'S218729122322', 'Kota Semarang', '2022-02-04 10:08:43', '2022-02-07 17:06:05', 'Selesai', 'S218729122322.pdf', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', 'Barang sudah diterima', 'S218729122322.png'),
(26, 'GP343232212', 'S218729131213', 'Kota Solo', '2022-02-04 10:22:18', '0000-00-00 00:00:00', 'Process', '', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', '', 'S218729131213.png'),
(27, 'GP203902323', 'S218721212342', 'Palembang', '2022-02-03 23:15:27', '2022-02-09 17:37:17', 'Selesai', 'S218721212342.png', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMA NEGERI 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', 'Selesai', 'S218721212342.png'),
(28, '-', 'S218729131121', 'Semarang', '2022-02-11 13:35:36', '2022-02-19 10:03:26', 'Selesai', 'S218729131121.pdf', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', 'Selesai', 'S218729131121.png'),
(29, 'GP203902323', 'S21872913123423', 'Kota Semarang', '2022-02-13 14:53:52', '2022-02-19 09:22:49', 'Selesai', 'S21872913123423.pdf', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', 'Selesai', 'S21872913123423.png'),
(30, 'GP203902323', 'S218233453543', 'Kota Bandung', '2022-02-01 15:12:30', '2022-02-19 09:58:12', 'Selesai', 'S218233453543.pdf', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', 'Sudah diterima', 'S218233453543.png'),
(31, '-', 'S21823423423', 'Kota Tangerang', '2022-02-19 16:11:05', '2022-02-19 10:15:06', 'Selesai', 'S21823423423.pdf', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMA NEGERI 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', '', 'S21823423423.png'),
(32, '-', 'S2187291342', 'Sigli', '2022-02-19 17:34:32', '0000-00-00 00:00:00', 'Process', '', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMA NEGERI 1 KOTA LANGSA', 'Jl. A Yani No.XXX', 'Kec. Langsa Baro, Kota Langsa', '', 'S2187291342.png'),
(33, '-', 'S21844349131213', 'Takengon', '2022-02-19 17:35:38', '0000-00-00 00:00:00', 'Process', '', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA TAKENGON', 'Jl. A Yani No.XXX', 'Kec. Takengon, Kota Takengon', '', 'S21844349131213.png'),
(34, '-', 'S2187343431213', 'Medan', '2022-02-19 17:36:43', '0000-00-00 00:00:00', 'Process', '', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA MEDAN', 'Jl. A Yani No.XXX', 'Kec. Medan', '', 'S2187343431213.png'),
(35, '-', 'S2183434312342', 'Kota Bekasi', '2022-02-19 18:06:10', '0000-00-00 00:00:00', 'Process', '', 'MOBIL BOX', 'BK 1234 GU', 'PT. GAWAI PELADEN BAROKAH', 'Jl. Bhayangkara Komplek Alam Sutra, Pakualam', 'Serpong Utara, Kota Tangerang Selatan, Banten 1532', 'SMAN 1 KOTA BEKASI', 'Jl. A Yani No.XXX ', 'Kec. Bekasi', '', 'S2183434312342.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tracing`
--

CREATE TABLE `tbl_tracing` (
  `trace_no` int(11) NOT NULL,
  `trace_resi` varchar(30) NOT NULL,
  `trace_kd_barang` varchar(30) DEFAULT NULL COMMENT 'GAK PAKE\r\n',
  `trace_lokasi` varchar(30) NOT NULL,
  `trace_tujuan` varchar(30) DEFAULT NULL COMMENT 'GAK PAKE',
  `trace_ket` text NOT NULL,
  `trace_updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_tracing`
--

INSERT INTO `tbl_tracing` (`trace_no`, `trace_resi`, `trace_kd_barang`, `trace_lokasi`, `trace_tujuan`, `trace_ket`, `trace_updated_at`) VALUES
(5, '223323', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-04 15:58:55'),
(35, 'S218729122322', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-07 23:03:25'),
(36, 'S218729122322', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-07 23:05:26'),
(37, 'S218729122322', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-07 23:05:33'),
(38, 'S218233312342', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-08 23:48:59'),
(39, 'S218233312342', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-08 23:49:44'),
(40, 'S218721212342', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-09 23:15:27'),
(41, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-09 23:17:48'),
(42, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Paket sampai di Kota Langsa', '2022-02-09 23:20:41'),
(43, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Dalam perjalanan (Checkpoint: Kota Langsa)', '2022-02-09 23:20:52'),
(44, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Dalam perjalanan (Melewati Titik Pemeriksaan [Checkpoint] : Kota Langsa)', '2022-02-09 23:22:50'),
(45, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Dalam perjalanan (Sedang melewati Checkpoint / Titik Pemeriksaan : Kota Langsa)', '2022-02-09 23:23:44'),
(46, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Dalam perjalanan (Sedang melewati Checkpoint / Titik Pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-09 23:24:40'),
(47, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Moda Transportasi dalam perjalanan (sedang melewati checkpoint / titik pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-09 23:26:22'),
(48, 'S218721212342', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-09 23:27:08'),
(49, 'S218721212342', NULL, 'Paket telah diterima. <a href=', NULL, '', '2022-02-09 17:34:27'),
(50, 'S218721212342', NULL, 'Paket telah diterima.', NULL, '<a href=\"upload/bast/S218721212342.png\">Klik disini untuk melihat BAST (Berita Acara Serah Terima)</a> ', '2022-02-09 17:35:41'),
(51, 'S218721212342', NULL, 'Paket telah diterima.', NULL, '<a href=\"http://localhost:8080/tracing/upload/bast/S218721212342.png\">Klik disini untuk melihat BAST (Berita Acara Serah Terima)</a> ', '2022-02-09 17:37:17'),
(52, 'S218729131121', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 13:35:36'),
(53, 'S218729131121', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:33:52'),
(54, 'S218729131213', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:34:07'),
(55, 'S218729131213', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:50:28'),
(56, 'S218729131213', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:50:31'),
(57, 'S218729122322', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:51:29'),
(58, 'S218729122322', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:51:32'),
(59, 'S218729122322', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:51:39'),
(60, 'S21872913123423', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 14:53:52'),
(61, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:53:58'),
(62, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:54:01'),
(63, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:54:03'),
(64, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:57:11'),
(65, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:57:14'),
(66, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:57:17'),
(67, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:58:12'),
(68, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 14:58:14'),
(69, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:01:40'),
(70, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:01:43'),
(71, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:01:45'),
(72, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:06:57'),
(73, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:11:08'),
(74, 'S21872913123423', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:11:15'),
(75, 'S218233453543', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 15:12:30'),
(76, 'S218233453543', NULL, 'Kota Langsa', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Langsa</b>)', '2022-02-19 15:12:46'),
(77, 'S21872913123423', NULL, 'Paket telah diterima', NULL, '<a target=\"_blank\" href=\"http://localhost:8080/tracing/upload/bast/S21872913123423.pdf\">Klik disini untuk melihat BAST (Berita Acara Serah Terima)</a> ', '2022-02-19 09:22:49'),
(78, 'S218233453543', NULL, 'Kota Banda Aceh', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Banda Aceh</b>)', '2022-02-19 15:57:04'),
(79, 'S218233453543', NULL, 'Paket telah diterima', NULL, '<a target=\"_blank\" href=\"http://localhost:8080/tracing/upload/bast/S218233453543.pdf\">Klik disini untuk melihat BAST (Berita Acara Serah Terima)</a> ', '2022-02-19 09:58:12'),
(80, 'S218729131121', NULL, 'Kota Banda Aceh', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Banda Aceh</b>)', '2022-02-19 16:03:12'),
(81, 'S218729131121', NULL, 'Paket telah diterima di kota t', NULL, '<a target=\"_blank\" href=\"http://localhost:8080/tracing/upload/bast/S218729131121.pdf\"><font color=\"#5F9EA0\">Klik disini untuk melihat BAST (Berita Acara Serah Terima)</font></a> ', '2022-02-19 10:03:26'),
(82, 'S21823423423', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 16:11:05'),
(83, 'S21823423423', NULL, 'Kota Banda Aceh', NULL, 'Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b>Kota Banda Aceh</b>)', '2022-02-19 16:13:50'),
(84, 'S21823423423', NULL, 'Paket telah diterima', NULL, '<a target=\"_blank\" href=\"http://localhost:8080/tracing/upload/bast/S21823423423.pdf\"><font color=\"#5F9EA0\">Klik disini untuk melihat BAST (Berita Acara Serah Terima)</font></a> ', '2022-02-19 10:15:06'),
(85, 'S2187291342', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 17:34:32'),
(86, 'S21844349131213', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 17:35:38'),
(87, 'S2187343431213', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 17:36:43'),
(88, 'S2183434312342', NULL, 'Tindak Lanjut', NULL, 'Sedang dalam proses tindak lanjut, menunggu muatan logistik', '2022-02-19 18:06:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(30) NOT NULL,
  `user_nama` varchar(30) NOT NULL,
  `user_role` varchar(2) NOT NULL COMMENT '1= Super Admin, 2=Admin, 3=user\r\n',
  `user_pwd` varchar(100) NOT NULL COMMENT 'md5',
  `user_wilayah` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_username`, `user_nama`, `user_role`, `user_pwd`, `user_wilayah`) VALUES
(10, 'admin_bandaaceh', 'Fitra Devia', '2', 'f3cdc853c9b3948366e7bef5691e6e18', 'Kota Banda Aceh'),
(13, 'admin_bandung', 'Awik', '2', 'bdad3e6bea68fffa9865b4b800abde5b', 'Kota Bandung'),
(7, 'admin_langsa', 'Ilham Ramadhan', '2', '44e90c40398c8470adf1b76305acfac4', 'Kota Langsa'),
(14, 'admin_malang', 'Saddam Ramadhan', '2', '06701f6c52201898b8a2a5998b246d28', 'Kota Malang'),
(12, 'admin_medan', 'Alfin', '2', '3bb20d4a414a9a9ea38ccb7f93565a48', 'Kota Medan'),
(8, 'admin_palembang', 'Sucita Dwi Ranti', '2', '8266c07620d18dc79d41eec274a79c51', 'Kota Palembang'),
(11, 'admin_sigli', 'Shiddiq', '2', 'f0f0a81db51feeaefedd5e4c9aa84550', 'Kota Sigli'),
(9, 'admin_tangerang', 'Reza Maulana', '2', '6b2c721459289d28b88f0abf858f59ab', 'Kota Tangerang'),
(15, 'superadmin', 'Zulfan Fauzi', '1', 'fdce6031b2f6f1655ed80945bceb02b8', 'Kota Langsa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang_logistik`
--
ALTER TABLE `tbl_barang_logistik`
  ADD PRIMARY KEY (`barang_no`);

--
-- Indexes for table `tbl_config`
--
ALTER TABLE `tbl_config`
  ADD PRIMARY KEY (`id_config`);

--
-- Indexes for table `tbl_resi`
--
ALTER TABLE `tbl_resi`
  ADD PRIMARY KEY (`resi_no`),
  ADD UNIQUE KEY `resi_kode` (`resi_kode`);

--
-- Indexes for table `tbl_tracing`
--
ALTER TABLE `tbl_tracing`
  ADD PRIMARY KEY (`trace_no`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_username`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang_logistik`
--
ALTER TABLE `tbl_barang_logistik`
  MODIFY `barang_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_resi`
--
ALTER TABLE `tbl_resi`
  MODIFY `resi_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_tracing`
--
ALTER TABLE `tbl_tracing`
  MODIFY `trace_no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
