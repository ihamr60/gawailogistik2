<link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Poppins:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />

<style type="text/css">
    body {
            font-family: Poppins;
        }

        a:link {
            color: white;
            text-decoration: none;
        }
        a:hover {
            color: green;
        }
        a:active {
            color: green;
        }
    
</style>

<style type="text/css">
        #loading {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: center no-repeat #fff;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .no-js #loader { display: none; }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }

        .loader {
            border: 10px solid #f3f3f3;
            border-radius: 50%;
            border-top: 10px solid #3498db;
            border-bottom: 10px solid #FFC107;
            width: 150px;
            height: 150px;
            left: 45%;
            top: 20%;
            -webkit-animation: spin 2s linear infinite;
            position: fixed;
            animation: spin 2s linear infinite;
        }

        .textLoader{
            position: fixed;
            top: 56%;
            left: 45.6%;
            color: #34495e;
        }
            
        /*-- responsive --*/
            @media screen and (max-width: 1034px){
                .textLoader{
                    left: 46.2%;
                }
            }

            @media screen and (max-width: 824px){
                .textLoader {
                    left: 47.2%;
                }
            }

            @media screen and (max-width: 732px){
                .textLoader {
                    left: 48.2%;
                }
            }

            @media screen and (max-width: 500px){
                .loader{
                    left: 36.5%;;
                }
                .textLoader {
                    left: 40.5%;
                }
            }

            @media screen and (max-height: 432px){
                .textLoader {
                    top: 65%;
                }
            }

            @media screen and (max-height: 350px){
                .textLoader {
                    top: 75%;
                }
            }

            @media screen and (max-height: 312px){
                .textLoader {
                    display: none;
                }
            }
        /*-- responsive --*/
    </style>