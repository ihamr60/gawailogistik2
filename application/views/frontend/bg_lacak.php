<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>PT GAWAI PELADEN BAROKAH</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="PT GAWAI PELADEN BAROKAH" name="description" />
    <meta content="PT GAWAI PELADEN BAROKAH" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components_front/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components_front/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components_front/animate.css/animate.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets_front/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets_front/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets_front/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets_front/css/theme_blue.min.css" />
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- RS5.0 Main Stylesheet -->
    <link href="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/css/settings.css" rel="stylesheet" />
    <!-- RS5.0 Layers and Navigation Styles -->
    <link href="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/css/layers.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/css/navigation.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components_front/flexslider/flexslider.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components_front/jquery-colorbox/example2/colorbox.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <?php $this->load->view('frontend/style');?>
</head>

<body>
    <?php $this->load->view('frontend/kontak_wa'); ?>
    <div id="loading" class="">
        <div class="logoLoader"></div>
        <span class="loader hidden-xs hidden-sm"></span>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <div class="">
            <center>
                <div class="container">
                    <b>S E D A N G - M E M U A T . . </b>
                </div>
            </center>
        </div>
    </div>
    <!-- start: HEADER -->
    <header>
        <!-- start: SLIDING BAR (SB) -->
        
        <!-- end: SLIDING BAR -->

        <!-- start: TOP BAR -->
        <div class="clearfix " id="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- start: TOP BAR CALL US -->
                        <div class="callus">
                            E-Mail :<a href="mailto:info@example.com">
                        gawaipeladenbarokah@gmail.com
                    </a>
                        </div>
                        <!-- end: TOP BAR CALL US -->
                    </div>
                    <div class="col-sm-6">
                        <!-- start: TOP BAR SOCIAL ICONS -->
                        <div class="social-icons">
                            <ul>
                                <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
                                    <a target="_blank" href="http://www.twitter.com">
                                Twitter
                            </a>
                                <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
                                    <a target="_blank" href="http://facebook.com">
                                Facebook
                            </a>
                                </li>
                                <li class="social-google tooltips" data-original-title="Google" data-placement="bottom">
                                    <a target="_blank" href="http://google.com">
                                Google+
                            </a>
                                </li>
                                <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
                                    <a target="_blank" href="http://linkedin.com">
                                LinkedIn
                            </a>
                                </li>
                                <li class="social-youtube tooltips" data-original-title="YouTube" data-placement="bottom">
                                    <a target="_blank" href="http://youtube.com/">
                                YouTube
                            </a>
                                </li>
                               
                            </ul>
                        </div>
                        <!-- end: TOP BAR SOCIAL ICONS -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end: TOP BAR -->
        <div role="navigation" class="navbar navbar-default navbar-fixed-top space-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <a style="font-family: 'Poppins'; color: black;" class="navbar-brand hidden-xs hidden-sm" href="<?php echo base_url() ?>index.php/frontend">
                       PT <b>GAWAI</b> PELADEN <b>BAROKAH</b>
                    </a>
                    <a style="font-family: 'Poppins'; color: black;" class="navbar-brand hidden-md hidden-lg hidden-xl" href="<?php echo base_url() ?>index.php/frontend">
                        <span><img src="<?php echo base_url() ?>vendor/assets_front/images/mobile-phone.png" width="30px"> PT <b>Gawai</b> PeladeN</span>
                    </a>
                    <!-- end: LOGO -->
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <!--class="active"-->
                            <a style="font-family: 'Poppins'; color: black;" href="<?php echo base_url() ?>index.php/frontend">
                                B E R A N D A
                            </a>
                        </li>
                        
                        <li class="dropdown">
                            <a style="font-family: 'Poppins'; color: black;" class="dropdown-toggle" href="<?php echo base_url() ?>index.php/frontend/bg_lacak">
                                L A C A K  - P E N G I R I M A N  
                            </a>
            
                        </li>
                        <li class="dropdown">
                            <a style="font-family: 'Poppins'; color: black;" target="_blank" class="dropdown-toggle" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016" >
                                S I P L A H  -  B L I B L I_ S T O R E
                            </a>
            
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
    </header>
    <!-- end: HEADER -->
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <!-- start: STYLE SELECTOR BOX -->
        <div id="style_selector" class="hidden-xs">
            <div id="style_selector_container">
                <div class="style-main-title">
                    Style Selector
                </div>
                <div class="box-title">
                    Predefined Color Schemes
                </div>
                <div class="images icons-color">
                    <a id="blue" href="#"><img class="active" alt="" src="assets_front/images/blue.png"></a>
                    <a id="purple" href="#"><img alt="" src="assets_front/images/purple.png"></a>
                    <a id="red" href="#"><img alt="" src="assets_front/images/red.png"></a>
                    <a id="orange" href="#"><img alt="" src="assets_front/images/orange.png"></a>
                    <a id="green" href="#"><img alt="" src="assets_front/images/green.png"></a>
                </div>
                <div style="height:25px;line-height:25px; text-align: center">
                    <a class="clear_style" href="#">
                Clear Styles
            </a>
                    <a class="save_style" href="#">
                Save Styles
            </a>
                </div>
            </div>
            <div class="style-toggle close"></div>
        </div>
        <!-- end: STYLE SELECTOR BOX -->
        <!-- start: REVOLUTION SLIDERS -->
        <div class="hidden-md hidden-lg hidden-xl">
            <img src="<?php echo base_url(); ?>vendor/assets_front/images/gudang2.jpg" style="width: 100%; height: 400px; background-color:rgb(246, 246, 246)" alt="slidebg1" data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
        </div>
       <br><br><br>
        <!-- end: REVOLUTION SLIDERS -->
        <section>
            <!-- start: CORE BOXES CONTAINER -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- start: SEARCH RESULT -->
                        <div class="search-classic">
                            <form method="post" action="<?php echo base_url() ?>index.php/frontend/lacak_logistik" class="form-inline">
                                <div style="width: 100%;" class="input-group well">
                                    <input type="text" style="width: 100%;" class="form-control" name="trace_resi" placeholder="SPK Number ...">
                                    <span class="input-group-btn">
                                        <button style="width: 100%; font-family: 'Poppins'" class="btn btn-primary" type="submit">
                                            <b>T R A C E</b>
                                        </button>
                                    </span>
                                </div>
                            </form>
                            <h3 style="text-align: center; font-family: 'Poppins'"><b>Tracking</b> Pengiriman Logistik</h3>
                            <hr>
                            <?php echo $this->session->flashdata('info'); ?>
                            <?php 
                                    foreach($data_spk->result_array() as $d)
                                    {
                                        $cek_spk = $this->web_app_model->getWhereOneItem($d['trace_resi'],'resi_kode','tbl_resi');
                            ?>
                            <div class="search-result">
                                <h4>
                                    <a style="font-family: 'Poppins'" href="#">
                                        <?php echo $d['trace_resi'] ?> - di <b><?php echo $d['trace_lokasi'] ?></b>
                                    </a>
                                </h4>
                                <a href="#">
                                    Updated at <?php echo $d['trace_updated_at'] ?> WIB | Tujuan Pengiriman : <b><?php echo $cek_spk['resi_tujuan'] ?></b>
                                </a>
                                <p>
                                    <font color="black"><b>UPDATE TERKINI :</b> <br><b><?php echo $d['trace_lokasi'] ?></b> | <?php echo $d['trace_ket'] ?></font>
                                </p>
                            </div>
                            <hr>
                        <?php } ?>
                         
                        <br><br><br>
                        </div>
                        <!-- end: SEARCH RESULT -->
                    </div>
                    
                </div>
            </div>
            <!-- end: CORE BOXES CONTAINER -->
        </section>
        
        
     
       
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <?php $this->load->view('frontend/kontak_us'); ?>
                </div>
                <div class="col-md-3">
                    
                </div>
                <div class="col-md-3">
                    <div class="newsletter">
                        <h4 style="text-align: center; font-family: 'Poppins'"><b>PT GAWAI PELADEN BAROKAH</b></h4>
                        <p>
                            Siap memenuhi kebutuhan mitra sekolah di seluruh INDONESIA. Beralihlah berasama kami dan dapatkan kenyamanan dalam berbelanja.
                        </p>
                        
                    </div>
                </div>
                
                
                <div class="col-md-2">
                    <h4 style="text-align: center; font-family: 'Poppins'"><b>FOLLOW US</b></h4>
                    <div class="social-icons">
                        <ul>
                            <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
                                <a target="_blank" href="http://www.twitter.com">
                                Twitter
                            </a>
                            </li>
                            <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
                                <a target="_blank" href="http://facebook.com" data-original-title="Facebook">
                                Facebook
                            </a>
                            </li>
                            <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
                                <a target="_blank" href="http://linkedin.com" data-original-title="LinkedIn">
                                LinkedIn
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                        <a class="logo" href="<?php echo base_url() ?>index.php/frontend">
                        GAWAI PB
                    </a>
                    </div>
                    <div class="col-md-7">
                        <p>
                            &copy; Copyright
                            <script>
                                document.write(new Date().getFullYear())
                            </script> by PT Gawai Peladen Barokah. All Rights Reserved.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <nav id="sub-menu">
                            <ul>
                                <li>
                                    <a href="#">
                                    FAQ's
                                </a>
                                </li>
                                <li>
                                    <a href="#">
                                    Sitemap
                                </a>
                                </li>
                                <li>
                                    <a href="#">
                                    Contact
                                </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <a id="scroll-top" href="#"><i class="fa fa-angle-up"></i></a>
    <!-- end: FOOTER -->

    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="../../bower_components_front/respond/dest/respond.min.js"></script>
        <script src="../../bower_components_front/html5shiv/dist/html5shiv.min.js"></script>
        <script src="../../bower_components_front/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery.transit/jquery.transit.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery_appear/jquery.appear.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets_front/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- RS5.0 Core JS Files -->
    <script src="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
    <script src="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/js/source/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components_front/flexslider/jquery.flexslider-min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components_front/jquery.stellar/jquery.stellar.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components_front/jquery-colorbox/jquery.colorbox-min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/assets_front/js/min/index.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            Index.init();
            $.stellar();
        });
    </script>
    <script type="text/javascript">
        $(window).load(function() {
            setTimeout(function(){
                $("#loading").hide();
                $(".loader").hide();
            },1000);
        });
    </script>

    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/62127246a34c245641273a05/1fsc0sp9u';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
    <!--End of Tawk.to Script-->
    
</body>

</html>