<div class="contact-details">
    <h4 style="text-align: center; font-family: 'Poppins'"><b>KONTAK KAMI</b></h4>
    <ul class="contact">
        <li>
            <p>
                <i class="fa fa-map-marker"></i><strong>Alamat :</strong> Jl. Bayangkara Pusdiklantas No.10, Pakualam, Kec. Serpong Utara, Kota Tangerang Selatan, Banten 15320
            </p>
        </li>
        <li>
            <p>
                <i class="fa fa-certificate"></i><strong>SIPLah Blibli :</strong> <a target="_blank" class="dropdown-toggle" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">Klik untuk kunjungi Toko</a>
            </p>
        </li>
        <li>
            <p>
                <i class="clip-phone"></i><strong>Telpon / SMS / WA:</strong>
                <a href="mailto:mail@example.com">
                +62-813-2121-5338
            </a>
            </p>
        </li>
        <li>
            <p>
                <i class="fa fa-envelope"></i><strong>Email:</strong>
                <a href="mailto:mail@example.com">
                gawaipeladenbarokah@gmail.com
            </a>
            </p>
        </li>
    </ul>
</div>