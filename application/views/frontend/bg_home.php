<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>PT GAWAI PELADEN BAROKAH</title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="PT GAWAI PELADEN BAROKAH" name="description" />
    <meta content="PT GAWAI PELADEN BAROKAH" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components_front/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components_front/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components_front/animate.css/animate.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets_front/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets_front/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets_front/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets_front/css/theme_blue.min.css" />
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- RS5.0 Main Stylesheet -->
    <link href="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/css/settings.css" rel="stylesheet" />
    <!-- RS5.0 Layers and Navigation Styles -->
    <link href="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/css/layers.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/css/navigation.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components_front/flexslider/flexslider.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components_front/jquery-colorbox/example2/colorbox.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <?php $this->load->view('frontend/style');?>
</head>

<body>
    <?php $this->load->view('frontend/kontak_wa'); ?>
    <div id="loading" class="">
        <div class="logoLoader"></div>
        <span class="loader hidden-xs hidden-sm"></span>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <div class="">
            <center>
                <div class="container">
                    <b>S E D A N G - M E M U A T . . </b>
                </div>
            </center>
        </div>
    </div>

    <!-- start: HEADER -->
    <header>
        <!-- start: SLIDING BAR (SB) -->
        
        <!-- end: SLIDING BAR -->

        <!-- start: TOP BAR -->
        <div class="clearfix " id="topbar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- start: TOP BAR CALL US -->
                        <div style="color: black;" class="callus">
                                 E-Mail :
                                <a href="mailto:gawai.peladen.jakarta@gmail.com">
                                   <font color="black"> gawaipeladenbarokah@gmail.com </font>
                                </a>
                        </div>
                        <!-- end: TOP BAR CALL US -->
                    </div>
                    <div class="col-sm-6">
                        <!-- start: TOP BAR SOCIAL ICONS -->
                        <div class="social-icons">
                            <ul>
                                <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
                                    <a target="_blank" href="http://www.twitter.com">
                                Twitter
                            </a>
                                <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
                                    <a target="_blank" href="http://facebook.com">
                                Facebook
                            </a>
                                </li>
                                <li class="social-google tooltips" data-original-title="Google" data-placement="bottom">
                                    <a target="_blank" href="http://google.com">
                                Google+
                            </a>
                                </li>
                                <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
                                    <a target="_blank" href="http://linkedin.com">
                                LinkedIn
                            </a>
                                </li>
                                <li class="social-youtube tooltips" data-original-title="YouTube" data-placement="bottom">
                                    <a target="_blank" href="http://youtube.com/">
                                YouTube
                            </a>
                                </li>
                               
                            </ul>
                        </div>
                        <!-- end: TOP BAR SOCIAL ICONS -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end: TOP BAR -->
        <div role="navigation" class="navbar navbar-default navbar-fixed-top space-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- start: LOGO -->
                    <a style="font-family: 'Poppins'; color: black;" class="navbar-brand hidden-xs hidden-sm" href="<?php echo base_url() ?>index.php/frontend">
                       PT <b>GAWAI</b> PELADEN <b>BAROKAH</b>
                    </a>
                    <a style="font-family: 'Poppins'; color: black;" class="navbar-brand hidden-md hidden-lg hidden-xl" href="<?php echo base_url() ?>index.php/frontend">
                        <span><img src="<?php echo base_url() ?>vendor/assets_front/images/mobile-phone.png" width="30px"> PT <b>Gawai</b> PeladeN</span>
                    </a>
                    <!-- end: LOGO -->
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <!--class="active"-->
                            <a style="font-family: 'Poppins'; color: black;" href="<?php echo base_url() ?>index.php/frontend">
                                B E R A N D A
                            </a>
                        </li>
                        
                        <li class="dropdown">
                            <a style="font-family: 'Poppins'; color: black;" class="dropdown-toggle" href="<?php echo base_url() ?>index.php/frontend/bg_lacak">
                                L A C A K  - P E N G I R I M A N  
                            </a>
            
                        </li>
                        <li class="dropdown">
                            <a style="font-family: 'Poppins'; color: black;" target="_blank" class="dropdown-toggle" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016" >
                                S I P L A H  -  B L I B L I_ S T O R E
                            </a>
            
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
    </header>
    <!-- end: HEADER -->
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <!-- start: STYLE SELECTOR BOX -->
        <div id="style_selector" class="hidden-xs">
            <div id="style_selector_container">
                <div class="style-main-title">
                    Style Selector
                </div>
                <div class="box-title">
                    Predefined Color Schemes
                </div>
                <div class="images icons-color">
                    <a id="blue" href="#"><img class="active" alt="" src="assets_front/images/blue.png"></a>
                    <a id="purple" href="#"><img alt="" src="assets_front/images/purple.png"></a>
                    <a id="red" href="#"><img alt="" src="assets_front/images/red.png"></a>
                    <a id="orange" href="#"><img alt="" src="assets_front/images/orange.png"></a>
                    <a id="green" href="#"><img alt="" src="assets_front/images/green.png"></a>
                </div>
                <div style="height:25px;line-height:25px; text-align: center">
                    <a class="clear_style" href="#">
                Clear Styles
            </a>
                    <a class="save_style" href="#">
                Save Styles
            </a>
                </div>
            </div>
            <div class="style-toggle close"></div>
        </div>
        <!-- end: STYLE SELECTOR BOX -->
        <!-- start: REVOLUTION SLIDERS -->
        <div class="hidden-md hidden-lg hidden-xl">
            <img src="<?php echo base_url(); ?>vendor/assets_front/images/sliders/landing.jpg" style="width: 100%; background-color:rgb(246, 246, 246)" alt="slidebg1" data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
        </div>
        <section class="fullwidthbanner-container hidden-xs hidden-sm">
            <div id="fullwidthabnner">
                <ul>
                    <!-- start: FIRST SLIDE -->
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url(); ?>vendor/assets_front/images/sliders/slidebg1.png" style="background-color:rgb(246, 246, 246)" alt="slidebg1" data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption lft slide_title slide_item_left" data-x="right" data-hoffset="-150" data-y="bottom" data-voffset="0" data-whitespace="normal" data-start="500">
                            <img src="<?php echo base_url(); ?>vendor/assets_front/images/paket4.png" alt="Image 1">
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div style="font-family: 'Poppins'"  class="tp-caption lft slide_title slide_item_left" data-x="left" data-hoffset="0" data-y="top" data-voffset="105" data-speed="400" data-start="1500" data-easing="easeOutExpo" data-width="full">
                            <b>Belanja</b> barang sekolah?
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div style="font-family: 'Poppins'"  class="caption tp-caption lft slide_subtitle slide_item_left" data-x="left" data-hoffset="0" data-y="top" data-voffset="180" data-speed="400" data-start="2000" data-easing="easeOutExpo" data-width="630px">
                            Sesuai Permendikbud Nomor 14 Tahun 2020 Tentang Pedoman PBJ
                        </div>
                        <!-- LAYER NR. 4 -->
                        <div style="font-family: 'Poppins'; color: black;" class="caption tp-caption lft slide_desc slide_item_left" data-x="left" data-hoffset="0" data-y="top" data-voffset="220" data-speed="400" data-start="2500" data-easing="easeOutExpo" data-width="full">
                            <b>PT. Gawai Peladen Barokah</b> siap memenuhi kebutuhan sekolah Anda
                            <br>dengan sistem dan manajemen pengiriman logistik yang lebih profesional. Dengan sistem
                            <br>checkpoint logistik di seluruh cabang perusahaan memudahkan Anda untuk melacak status pengiriman
                        </div>
                        <!-- LAYER NR. 5 -->
                        <a style="font-family: 'Poppins'" class="tp-caption lft btn btn-green slide_btn slide_item_left" target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016" data-x="left" data-hoffset="0" data-y="top" data-voffset="320" data-speed="400"
                            data-start="3000" data-easing="easeOutExpo">
                    Kunjungi <b>SIPlah Blibli Store</b> kami, sekarang!
                </a>
                    </li>
                    <!-- end: FIRST SLIDE -->
                    <!-- start: DUA SLIDE -->
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url(); ?>vendor/assets_front/images/sliders/slidebg1.png" style="background-color:rgb(246, 246, 246)" alt="slidebg1" data-bgfit="cover" data-bgposition="left bottom" data-bgrepeat="no-repeat">
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption lft slide_title slide_item_left" data-x="right" data-hoffset="-150" data-y="bottom" data-voffset="0" data-whitespace="normal" data-start="500">
                            <img src="<?php echo base_url(); ?>vendor/assets_front/images/truck2.png" alt="Image 1">
                        </div>
                        <!-- LAYER NR. 2 -->
                        <div style="font-family: 'Poppins'" class="tp-caption lft slide_title slide_item_left" data-x="left" data-hoffset="0" data-y="top" data-voffset="105" data-speed="400" data-start="1500" data-easing="easeOutExpo" data-width="full">
                            LOGISTICS <b>TRACKING</b> SYSTEM
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div style="font-family: 'Poppins'" class="caption tp-caption lft slide_subtitle slide_item_left" data-x="left" data-hoffset="0" data-y="top" data-voffset="180" data-speed="400" data-start="2000" data-easing="easeOutExpo" data-width="360px">
                            SISTEM MANAJEMEN LOGISTIK TERPUSAT
                        </div>
                        <!-- LAYER NR. 4 -->
                        <div style="font-family: 'Poppins'; color: black;" class="caption tp-caption lft slide_desc slide_item_left" data-x="left" data-hoffset="0" data-y="top" data-voffset="220" data-speed="400" data-start="2500" data-easing="easeOutExpo" data-width="full">
                           Dengan kehandalan manajemen <b>PT. GAWAI PELADEN BAROKAH</b>
                            <br>pelayanan terbaik akan diberikan kepada Anda
                            <br>mendukung sistem informasi digital yang memudahkan pelanggan dalam monitoring logistik
                        </div>
                        <!-- LAYER NR. 5 -->
                        <a style="font-family: 'Poppins'" class="tp-caption lft btn btn-green slide_btn slide_item_left" target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016" data-x="left" data-hoffset="0" data-y="top" data-voffset="320" data-speed="400"
                            data-start="3000" data-easing="easeOutExpo">
                    Kunjungi <b>SIPlah Blibli Store</b> kami, sekarang!
                </a>
                    </li>
                    <!-- end: DUA SLIDE -->
                    <!-- start: SECOND SLIDE -->

                    <!-- end: SECOND SLIDE -->
                    <!-- start: THIRD SLIDE -->
                    
                    <!-- end: THIRD SLIDE -->
                </ul>
            </div>
        </section>
        <!-- end: REVOLUTION SLIDERS -->
        <section>
            <!-- start: CORE BOXES CONTAINER -->
            <div class="container hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
                                <i class="clip-checkbox circle-icon"></i>
                                <h2 style="font-family: 'Poppins'"><b>ALL PRODUK</b></h2>
                            </div>
                            <div class="content">
                                Kami menyediakan segala jenis produk yang Anda inginkan, hubungi CS kami maka semua produk 
                                yang diinginkan akan tersedia di SIPlah Bliblik
                            </div>
                            <a target="_blank" class="view-more" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                Open SIPlah Blibli <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
                                <i class="clip-checkbox circle-icon"></i>
                                <h2 style="font-family: 'Poppins'"><b>PELAYANAN RESPONSIF</b></h2>
                            </div>
                            <div class="content">
                                Pelayanan responsif menjadi prioritas utama kami dalam melayani mitra, kami memiliki moda
                                transportasi sendiri yang memudahkan proses pengiriman
                            </div>
                            <a target="_blank" class="view-more" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                Open SIPlah Blibli <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="core-box">
                            <div class="heading">
                                <i class="clip-checkbox circle-icon"></i>
                                <h2 style="font-family: 'Poppins'"><b>PENGIRIMAN UP-TO-DATE</b></h2>
                            </div>
                            <div class="content">
                                Dengan pemanfaatan teknologi informasi, toko kami memiliki keunggulan dimana setiap mitra
                                dapat melacak secara mandiri status pengiriman logistik
                            </div>
                            <a target="_blank" class="view-more" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                Open SIPlah Blibli <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: CORE BOXES CONTAINER -->
        </section>
        <section class="wrapper wrapper-grey padding50">
            <!-- start: WORKS CONTAINER -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 style="font-family: 'Poppins'; color: black;" class="center">O U R  -  <b>C A T A L O G</b></h2>
                        <hr>
                        <p class="center">
                            Toko kami mengusung konsep penjualan On Demand, yang mana kami memprioritaskan kebutuhan mitra sekolah dalam hal pengadaan barang. Kami tidak fokus pada satu produk. Tapi kami menyediakan semua kebutuhan barang mitra sekolah. Berikut adalah sekilas barang yang pernah dipesan atau kunjungi toko kami di <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016"><b>SIPLah Blibli</b></a> untuk detail dan proses pemesanan.
                            
                        </p>
                        <div class="grid-container animate-group" data-animation-interval="100">
                            <div class="col-md-3 col-sm-6">
                                <div class="grid-item animate">
                                    <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                        <div class="grid-image">
                                            <img src="<?php echo base_url(); ?>vendor/assets_front/images/laptop.jpeg" class="img-responsive" />
                                            <span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
                                        </div>
                                        <div class="grid-content">
                                           <p align="center">LAPTOP ASUS TUF GAMING</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="grid-item animate">
                                    <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                        <div class="grid-image">
                                            <img src="<?php echo base_url(); ?>vendor/assets_front/images/meja2.jpg" class="img-responsive" />
                                            <span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
                                        </div>
                                        <div class="grid-content">
                                            <p align="center">KURSI KELAS SEKOLAH</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="grid-item animate">
                                    <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                        <div class="grid-image">
                                            <img src="<?php echo base_url(); ?>vendor/assets_front/images/papantulis.jpg" class="img-responsive" />
                                            <span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
                                        </div>
                                        <div class="grid-content">
                                            <p align="center">PAPAN TULIS SEKOLAH</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="grid-item animate">
                                    <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                        <div class="grid-image">
                                            <img src="<?php echo base_url(); ?>vendor/assets_front/images/sofa.jpeg" class="img-responsive" />
                                            <span class="image-overlay"> <i class="fa fa-mail-forward circle-icon circle-main-color"></i> </span>
                                        </div>
                                        <div class="grid-content">
                                            <p align="center">SHOFA RUANGAN KEP. SEKOLAH</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <p class="center">
                            <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
                                <b>CLICK HERE!</b> TO SEE MORE CATALOG ...
                            </a>
                        </p>
                    </div>
                </div>
            </div>
            <!-- end: WORKS CONTAINER -->
        </section>
        <section class="wrapper padding50 hidden-sm hidden-xs">
            <!-- start: ABOUT US CONTAINER -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 style=" text-align: center; font-family: 'Poppins'" style="text-align: right;"><b>ABOUT</b> US</h2>
                        <hr class="fade-left">
                        <p style="text-align: right;">
                            <b>PT Gawai Peladen Barokah</b> merupakan perusahaan (Perseroan Terbatas) yang bergerak dibidang Pengadaan Barang dan Jasa. 
                        </p>
                        <p style="text-align: right;">
                            Perusahaan skala nasional yang beralamat di Jl. Bayangkara Pusdiklantas No.10, Pakualam, Kec. Serpong Utara, Kota Tangerang Selatan, Banten 15320 adalah perusahaan yang bekerja dengan manajemen yang handal dan uptodate. 
                        </p>
                        <p style="text-align: right;">
                            Dengan sistem manajemen logistik yang berbasikan IT memberikan kenyamanan setiap pelanggan yang bermitra dengan kami
                        </p>
                        <hr class="fade-left">
                        <a style="font-family: 'Poppins'"  target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016" class="btn btn-default pull-right"><i class="fa fa-info"></i> Mulai Pemesanan ...</a>
                    </div>
                    <div class="col-sm-6">
                        <ul class="icon-list animate-group">
                            <li>
                                <div class="timeline animate" data-animation-options='{"animation":"scaleToBottom", "duration":"300"}'></div>
                                <i class="clip-note circle-icon circle-teal animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'></i>
                                <div class="icon-list-content">
                                    <h4 style="font-family: 'Poppins'"><b>ORDER</b> BARANG</h4>
                                    <p>
                                        Mitra sekolah melakukan order pemesanan terlebih dahulu melalui SIPLah Blibli
                                        sesuai dengan PERMENDIKBUD No. 14 Tahun 2020<br>
                                        <a target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">Klik disini untuk order via SIPLah Blibli</a>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="timeline animate" data-animation-options='{"animation":"scaleToBottom", "duration":"300", "delay": "300"}'></div>
                                <i class="fa fa-dropbox circle-icon circle-bricky animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'></i>
                                <div class="icon-list-content">
                                    <h4 style="font-family: 'Poppins'"><b>PROSES</b> PACKING</h4>
                                    <p>
                                        Setelah proses pemesanan berhasil dilakukan melalui platform SIPLah blibli,
                                        petugas kami akan melakukan checking dan packing.
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="timeline animate" data-animation-options='{"animation":"scaleToBottom", "duration":"300", "delay": "300"}'></div>
                                <i class="clip-paperplane circle-icon circle-green animate" data-animation-options='{"animation":"flipInY", "duration":"600"}'></i>
                                <div class="icon-list-content">
                                    <h4 style="font-family: 'Poppins'"><b>PENGIRIMAN</b> LOGISTIK</h4>
                                    <p>
                                        Paket logistik kemudian masuk dalam tahap pengiriman, pada tahap ini mitra
                                        sekolah dapat melacak status pengiriman secara online melalui website resmi kami 
                                        pada menu "LACAK PENGIRIMAN" di atas
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end: ABOUT US CONTAINER -->
        </section>
        <section class="wrapper wrapper-grey padding50">
            <!-- start: PORTFOLIO CONTAINER -->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="flexslider" data-plugin-options='{"controlNav":false,"sync": "#carousel"}'>
                            <ul class="slides">
                                <li>
                                    <a class="group1" href="<?php echo base_url(); ?>vendor/assets_front/images/young.jpg" title="Caption here">
                                        <img src="<?php echo base_url(); ?>vendor/assets_front/images/young.jpg" />
                                        <span class="image-overlay"> <i class="clip-expand circle-icon circle-main-color"></i> </span>
                                    </a>
                                </li>
                                <li>
                                    <a class="group1" href="<?php echo base_url(); ?>vendor/assets_front/images/driver.jpg" title="Caption here">
                                        <img src="<?php echo base_url(); ?>vendor/assets_front/images/driver.jpg" />
                                        <span class="image-overlay"> <i class="clip-expand circle-icon circle-main-color"></i> </span>
                                    </a>
                                </li>
                                
                            </ul>
                        </div>
                        <div id="carousel" class="flexslider animate-group" data-plugin-options='{"itemWidth": 120, "itemMargin": 5}'>
                            <ul class="slides">
                                <li>
                                    <img src="<?php echo base_url(); ?>vendor/assets_front/images/young.jpg" class="animate" data-animation-options='{"animation":"fadeIn", "duration":"600"}' />
                                </li>
                                <li>
                                    <img src="<?php echo base_url(); ?>vendor/assets_front/images/driver.jpg" class="animate" data-animation-options='{"animation":"fadeIn", "duration":"600"}' />
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="section-content">
                            <br>
                            <h2 style="text-align: center; font-family: 'Poppins'" ><b>WHY</b> CHOOSE US?</h2>
                            <hr class="fade-right">
                            <p>
                                <b>PT GAWAI PELADEN BAROKAH</b> adalah perusahaan skala nasional yang berlokasi di Tangerang Banten.
                            </p>
                            <p>
                                Prioritas kebutuhan mitra sekolah adalah prioritas utama kami. Sehingga kami akan selalu menyediakan apapun kebutuhan mitra sekolah
                            </p>
                            <p>
                                Mitra sekolah bahkan dapat request permintaan barang yang tidak tersedia di toko SIPLah Blibli agar dapat kami prepare persediaan barang tersebut melalui SIPLah Blibli
                            </p>
                            <ul>
                                <li>
                                    Keunggulan layanan kami adalah mitra sekolah akan selalu dapat memonitoring status perjalan logistik Anda melalui fitur <b>LACAK PENGIRIMAN</b>
                                </li>
                                <li>
                                    Keamanan dan kualitas barang menjadi tugas utama kami dalam melakukan misi pengiriman barang sampai ke lokasi.
                                </li>
                                <li>
                                    <b>KEBUTUHAN BARANG APAPUN?</b>
                                </li>
                                <li>
                                    <b>PT GAWAI PELADEN BAROKAH</b> selalu siap menyediakan kebutuhan Anda..
                                </li>
                                <li>
                                    Ayo berbelanja kebutuhan sekolah Anda melalui Toko SIPLah Blibli kami dengan Klink Navigasi dibawah
                                </li>
                            </ul>
                            <hr class="fade-right">
                            <p align="center"><a style="font-family: 'Poppins'" target="_blank" href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016" class="btn btn-default"><i class="fa fa-info"></i> Open SIPLah Blibli Store ...</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end: PORTFOLIO CONTAINER -->
        </section>
        <section class="wrapper padding50">
            <!-- start: CLIENTS CONTAINER -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="center" style="font-family: 'Poppins'" ><b>APA KATA MEREKA?</b></h2>
                        <h4 class="center" style="font-family: 'Poppins'" >Cukup oke, dibandingkan dengan Toko SIPlah Blibli sebelah, belanja di PT GAWAI memuaskan pihak sekolah karena barang dan status perjalanan logistik nya jelas. Bisa di monitor secara online dan sesuai SOP.</h4>
                    </div>
                    
                </div>
            </div>
            <!-- end: CLIENTS CONTAINER -->
        </section>
        
     
       
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <?php $this->load->view('frontend/kontak_us'); ?>
                </div>
                <div class="col-md-1">
                    
                </div>
                <div class="col-md-5">
                    <div class="newsletter">
                        <h4 style="text-align: center; font-family: 'Poppins'"><b>PT GAWAI PELADEN BAROKAH</b></h4>
                        <p>
                            Siap memenuhi kebutuhan mitra sekolah di seluruh INDONESIA. Beralihlah berasama kami dan dapatkan kenyamanan dalam berbelanja.
                        </p>
                        
                    </div>
                </div>
                
                
                <div class="col-md-2">
                    <h4 style="text-align: center; font-family: 'Poppins'"><b>FOLLOW US</b></h4>
                    <div class="social-icons">
                        <ul>
                            <li class="social-twitter tooltips" data-original-title="Twitter" data-placement="bottom">
                                <a target="_blank" href="http://www.twitter.com">
                                Twitter
                            </a>
                            </li>
                            <li class="social-facebook tooltips" data-original-title="Facebook" data-placement="bottom">
                                <a target="_blank" href="http://facebook.com" data-original-title="Facebook">
                                Facebook
                            </a>
                            </li>
                            <li class="social-linkedin tooltips" data-original-title="LinkedIn" data-placement="bottom">
                                <a target="_blank" href="http://linkedin.com" data-original-title="LinkedIn">
                                LinkedIn
                            </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                        <a class="logo" href="<?php echo base_url() ?>index.php/frontend">
                        GAWAI PB
                    </a>
                    </div>
                    <div class="col-md-7">
                        <p>
                            &copy; Copyright
                            <script>
                                document.write(new Date().getFullYear())
                            </script> by PT Gawai Peladen Barokah. All Rights Reserved.
                        </p>
                    </div>
                    <div class="col-md-4">
                        <nav id="sub-menu">
                            <ul>
                                <li>
                                    <a href="#">
                                    FAQ's
                                </a>
                                </li>
                                <li>
                                    <a href="#">
                                    Sitemap
                                </a>
                                </li>
                                <li>
                                    <a href="#">
                                    Contact
                                </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--<a id="scroll-top" href="#"><i class="fa fa-angle-up"></i></a>-->
    <!-- end: FOOTER -->

    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="../../bower_components_front/respond/dest/respond.min.js"></script>
        <script src="../../bower_components_front/html5shiv/dist/html5shiv.min.js"></script>
        <script src="../../bower_components_front/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery.transit/jquery.transit.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery_appear/jquery.appear.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components_front/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets_front/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- RS5.0 Core JS Files -->
    <script src="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
    <script src="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/js/source/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/assets_front/plugins/slider-revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components_front/flexslider/jquery.flexslider-min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components_front/jquery.stellar/jquery.stellar.min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/bower_components_front/jquery-colorbox/jquery.colorbox-min.js"></script>
    <script src="<?php echo base_url(); ?>vendor/assets_front/js/min/index.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script>
        jQuery(document).ready(function() {
            Main.init();
            Index.init();
            $.stellar();
        });
    </script>
    <script type="text/javascript">
        $(window).load(function() {
            setTimeout(function(){
                $("#loading").hide();
                $(".loader").hide();
            },1000);
        });
    </script>

    <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/62127246a34c245641273a05/1fsc0sp9u';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
            })();
        </script>
    <!--End of Tawk.to Script-->

</body>

</html>