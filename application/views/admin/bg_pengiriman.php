<?php
    function rupiah($angka){
            $hasil_rupiah = "<b>Rp</b> " . number_format($angka,0,',','.');
            return $hasil_rupiah;
        }

?>

<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?php echo base_url() ?>index.php/admin?home=1">
                                    Home
                                </a>
                            </li>
                            <li class="active">
                                Data Produk
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <div class="row">
                    <div class="col-md-7" style="position: sticky; top: 70px;">
                        <?php //echo $this->session->flashdata('info'); ?>

                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12 space20">
                                         <div class="modal-header">
                                            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/database.png">&nbsp;&nbsp;&nbsp;<b>DATA CENTER</b> LOGISTIK PT. GAWAI PELADEN BAROKAH</h4>
                                        </div>
                                        <br>
                                        <a data-toggle="modal" href="#modalTambahLogistik" style="width:73%; background-color: #F0F8FF;" class="btn btn-default">
                                            <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/add-button.png">&nbsp;&nbsp;&nbsp; <font color="black">TAMBAH LOGISTIK</font>
                                        </a>
                                        <a data-toggle="modal" href="<?php echo base_url() ?>index.php/admin/bg_pengiriman?kirim=1" style="width:25%; float: right; background-color: #F0F8FF;" class="btn btn-default">
                                        <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/magnifying-glass.png">&nbsp;&nbsp;&nbsp; <font color="black">RESET</font>
                                    </a>
                                    <br>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover table-pengiriman" id="sample-table-2">
                                        <thead>
                                            <tr>
                                                <!--<th width="1" class="col-to-export center">#</th>-->
                                              <!--  <th width="" class="col-to-export">SPK/RESI</th> -->
                                                <th width="300" class="col-to-export">SPK / DESTINATION</th>
                                                <th width="100" class="col-to-export center">CREATED</th>
                                                <th width="100" class="col-to-export center">PENGIRIMAN</th>
                                                <th width="100" class="col-to-export center">INVOICE</th>
                                                <th width="1" class="center">#</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php
                            if(!empty($this->uri->segment(3)))
                            {
                        ?>
                        <div class="alert alert-block alert-info fade in">
                            <h4 class="alert-heading"><i class="fa fa-info-circle"></i> PETUNJUK OPERASIONAL!</h4>
                            <p>
                                Gunakan search engine pada tabel untuk mempercepat pencarian logistik. Lacak lokasi dan detail barang logistik dengan menggunakan menu dropdown hijau pada tabel. Gunakan fungsi reset untuk menampilkan kembali seluruh data logistik
                            </p>
                        </div>
                    <?php } ?>
                    </div>
                    <div class="col-md-5" style="position: sticky; top: 70px;">
                        <?php echo $this->session->flashdata('info'); ?>
                        <?php 
                            if(!empty($this->uri->segment(3)))
                            {
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="modal-header">
                                    <h4 style='text-transform: uppercase;' class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/file-and-folder.png">&nbsp;&nbsp;&nbsp;SPK : <b><?php echo $this->uri->segment(3) ?></b> | DOKUMEN PENTING</h4>
                                </div>
                                <br>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <a 
                                            <?php  
                                                if($data_spk['resi_bast'] == '')
                                                {
                                                    echo 'onclick="return false;"';
                                                }
                                            ?> 
                                            target="_blank" href="<?php echo base_url() ?>upload/bast/<?php echo $data_spk['resi_bast'] ?>">
                                            <p align="center">
                                                <img
                                                    <?php
                                                        if($data_spk['resi_bast'] == '')
                                                        {
                                                            echo 'style="filter: brightness(60%);"'; 
                                                        }
                                                    ?>
                                                    width="50px" src="<?php echo base_url() ?>vendor/assets/images/web/google-docs-2.png"><br><br>
                                                
                                                <?php
                                                    if($data_spk['resi_bast'] != '')
                                                    {
                                                        echo '<font color="black">BUKTI TERIMA</font><br>
                                                        <font color="#1E90FF"><b>AVAILABLE</b></font>';
                                                    }
                                                    else
                                                    {
                                                        echo '<font color="black"><s>BUKTI TERIMA</s></font><br>
                                                        <font color="red"><b>UNAVAILABLE</b></font>';
                                                    }
                                                ?>
                                            </p>
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a 
                                            <?php  
                                                if($data_spk['resi_dok_invoice'] == '')
                                                {
                                                    echo 'onclick="return false;"';
                                                }
                                            ?> 
                                            target="_blank" href="<?php echo base_url() ?>upload/invoice/<?php echo $data_spk['resi_dok_invoice'] ?>">
                                            <p align="center">
                                                <img
                                                    <?php
                                                        if($data_spk['resi_dok_invoice'] == '')
                                                        {
                                                            echo 'style="filter: brightness(60%);"'; 
                                                        }
                                                    ?>
                                                    width="50px" src="<?php echo base_url() ?>vendor/assets/images/web/google-docs-2.png"><br><br>
                                                
                                                <?php
                                                    if($data_spk['resi_dok_invoice'] != '')
                                                    {
                                                        echo '<font color="black">I N V O I C E</font><br>
                                                        <font color="#1E90FF"><b>AVAILABLE</b></font>';
                                                    }
                                                    else
                                                    {
                                                        echo '<font color="black"><s>I N V O I C E</s></font><br>
                                                        <font color="red"><b>UNAVAILABLE</b></font>';
                                                    }
                                                ?>
                                            </p>
                                        </a>
                                    </div>
                                    <div class="col-md-4">
                                        <a target="_blank" href="<?php echo base_url() ?>index.php/admin/cetak_surat_jalan/<?php echo $this->uri->segment(3) ?>">
                                            <p align="center">
                                                    <img width="50px" src="<?php echo base_url() ?>vendor/assets/images/web/google-docs-2.png"><br><br>
                                                <font color="black">SURAT JALAN</font><br>
                                                <font color="#1E90FF"><b>AVAILABLE</b></font>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <a 
                                            <?php  
                                                if($data_spk['resi_dok_kwitansi'] == '')
                                                {
                                                    echo 'onclick="return false;"';
                                                }
                                            ?> 
                                            target="_blank" href="<?php echo base_url() ?>upload/kwitansi/<?php echo $data_spk['resi_dok_kwitansi'] ?>">
                                            <p align="center">
                                                <img
                                                    <?php
                                                        if($data_spk['resi_dok_kwitansi'] == '')
                                                        {
                                                            echo 'style="filter: brightness(60%);"'; 
                                                        }
                                                    ?>
                                                    width="50px" src="<?php echo base_url() ?>vendor/assets/images/web/google-docs-2.png"><br><br>
                                                
                                                <?php
                                                    if($data_spk['resi_dok_kwitansi'] != '')
                                                    {
                                                        echo '<font color="black">K W I T A N S I</font><br>
                                                        <font color="#1E90FF"><b>AVAILABLE</b></font>';
                                                    }
                                                    else
                                                    {
                                                        echo '<font color="black"><s>K W I T A N S I</s></font><br>
                                                        <font color="red"><b>UNAVAILABLE</b></font>';
                                                    }
                                                ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="panel panel-default">
                            <div class="panel-body">
                                <div class="modal-header">
                                    <h4 style='text-transform: uppercase;' class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/package.png">&nbsp;&nbsp;&nbsp;SPK : <b><?php echo $this->uri->segment(3) ?></b> | MUATAN BARANG</h4>
                                </div>
                                <br>
                                <a data-toggle="modal" href="#modalTambahBarang" style="width:100%; background-color: #F0F8FF;" class="btn btn-default">
                                            <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/add-button.png">&nbsp;&nbsp;&nbsp; <font color="black">TAMBAH ITEM</font>
                                        </a>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-barang" id="sample-table-2">
                                            <thead>
                                                <tr>
                                                    <th width="1" class="col-to-export">NO</th>
                                                    <th width="1" class="col-to-export">NAMA BARANG</th>
                                                    <th width="1" class="col-to-export">QTY</th>
                                                    <th width="1" class="col-to-export">TOTAL</th>
                                                    <th width="1" class="center"></th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th width="1" colspan="" class="col-to-export">TOTAL</th>
                                                    <th></th>
                                                    <th></th>
                                                    <th width="1" class="center"><?php echo rupiah($total_harga['sum']) ?></th>
                                                    <th width="1" class="center"></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>-->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="modal-header">
                                    <h4 class="modal-title"> <img width="35px" src="<?php echo base_url()?>vendor/assets/images/web/truck.png">&nbsp;&nbsp;&nbsp; SPK : <b><?php echo $this->uri->segment(3) ?></b> | TRACKING STATS</h4>
                                </div>
                                
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-tracking" id="sample-table-2">
                                            <thead>
                                                <tr>
                                                    <th width="" class="col-to-export center">TIME</th>
                                                    <th width="" class="col-to-export">LOKASI TERKINI</th>
                                                    <th width="" class="col-to-export">KETERANGAN</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }
                        else 
                    { ?>
                        <div class="alert alert-block alert-info fade in hidden-xs hidden-sm">
                            <h4 class="alert-heading"><i class="fa fa-info-circle"></i> PETUNJUK OPERASIONAL!</h4>
                            <p>
                                Gunakan search engine pada tabel untuk mempercepat pencarian logistik. Lacak lokasi dan detail barang logistik dengan menggunakan menu dropdown hijau pada tabel. Gunakan fungsi reset untuk menampilkan kembali seluruh data logistik
                            </p>
                        </div>
                        <div class="panel panel-default hidden-xs hidden-sm">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>LABELS STATS</th>
                                        <th>KETERANGAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><span class="label label-default"> PROCESS</span></td>
                                        <td><code> Sedang dalam pengiriman </code></td>
                                    </tr>
                                    <tr>
                                        <td>S E L E S A I</td>
                                        <td><code> Sudah sampai tujuan </code></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label label-yellow"><font color="black">WAITING INVOICE</font></span></td>
                                        <td><code> Sedang menunggu unggahan invoice </code></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label label-purple"> WAITING PAYMENT</span></td>
                                        <td><code> Sudah mengunggah invoice, sedang menunggu pembayaran </code></td>
                                    </tr>
                                    <tr>
                                        <td><span class="label label-green"> F U L L - P A Y M E N T</span></td>
                                        <td><code> Pembayaran Lunas </code></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                        <div class="panel panel-default hidden-xs hidden-sm">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="35%">REPORTING</th>
                                        <th>VALUES DESCRIPTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>TARGET</b> PENAGIHAN </td>
                                        <td><code> <b><?php echo rupiah($target_penerimaan['target_penerimaan'])?></b> </code></td>
                                    </tr>
                                    <tr>
                                        <td>TOTAL <b>TERTAGIH</b></td>
                                        <td><code> <b><?php echo rupiah($penerimaan_saat_ini['penerimaan_saat_ini'])?></b> </code></td>
                                    </tr>
                                    <tr>
                                        <td>BELUM <b>TERTAGIH</b></td>
                                        <td><code> <b><?php echo rupiah($belum_diterima['belum_diterima'])?></b> </code></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    <?php } ?>

                    </div>
                    
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
            <!-- MODAL TAMBAH DATA -->
            <?php echo $modalTambahBarang; ?>
            <?php echo $modalTambahLogistik; ?>
            <?php //echo $modalUploadInvoice; ?>
            <!-- END MODAL TAMBAH DATA -->
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->
    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
    <!-- start: RIGHT SIDEBAR -->
    	<!-- ISI RIGHT SIDE BAR -->
    <!-- end: RIGHT SIDEBAR -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-modals.min.js"></script>

    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/pdfmake/build/vfs_fonts.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/table-export.min.js"></script>
    
    
    <script src="<?php echo base_url();?>vendor/bower_components/twbs-pagination/jquery.twbsPagination.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.pulsate/jquery.pulsate.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-elements.min.js"></script>

    <!--BUTTON-->
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/spin.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/ui-buttons.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/jquery.mask.js"></script>


    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/item-series.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
           // TableExport.init();
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            // Format mata uang.
            $( '.uang' ).mask('000.000.000', {reverse: true});

        })
    </script>

    <script type="text/javascript">

        $(".table-barang").DataTable({
            "iDisplayLength": 100000000000000,
            dom: 'Bfrtip',
                /*buttons: [
                    'print'
                ],*/
                buttons: [
            { extend: 'print', footer: true },
            { extend: 'excelHtml5', footer: true }
        ],
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_barang/'.$this->uri->segment(3).'') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 3,
                    "orderable": false
                }
            ],
            order: [
                [0, "desc" ]
            ],
        });

    </script>


    <script type="text/javascript">

        $(".table-pengiriman").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_pengiriman/'.$this->uri->segment(3).'') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 4,
                    "orderable": false
                }
            ],
            order: [
                [1, "desc" ]
            ],
        });
        
        $(".table-tracking").DataTable({
            processing: true,
            serverSide: true,
            ajax: {
              url: "<?php echo base_url('index.php/admin/data_tracking/'.$this->uri->segment(3).'') ?>",
              type:'POST',
            },
            columnDefs: [
                {
                    "targets": 2,
                    "orderable": false
                }
            ],
            order: [
                [0, "desc" ]
            ],
        });
    </script>


<!--
    <script type="text/javascript">
        Highcharts.chart('container', {

  chart: {
    type: 'item'
  },

  title: {
    text: '<b>TOTAL PRODUK / KATEGORI</b>'
  },

  subtitle: {
    text: 'Akumulasi seluruh produk terdaftar'
  },

  legend: {
    labelFormat: '{name} <span style="opacity: 0.4">{y}</span>'
  },

  series: [{
    name: 'Jumlah Item',
    keys: ['name', 'y', 'color', 'label'],
    data: [
    <?php 
        foreach($grafik_kategori->result_array() as $d)
        {
            if($d['kategori_no'] == 2)
            {
                echo "['".$d['kategori_nama']."', ".$d['total'].", '#DC143C', '".$d['kategori_nama']."'],";
            }
            else if($d['kategori_no'] == 3)
            {
                echo "['".$d['kategori_nama']."', ".$d['total'].", 'green', '".$d['kategori_nama']."'],";
            }
            else
            {
                echo "['".$d['kategori_nama']."', ".$d['total'].", 'orange', '".$d['kategori_nama']."'],";
            }
            
        }
    ?>
      
    ],
    dataLabels: {
      enabled: true,
      format: '{point.label}'
    },

    // Circular options
    center: ['50%', '88%'],
    size: '170%',
    startAngle: -100,
    endAngle: 100
  }],

  responsive: {
    rules: [{
      condition: {
        maxWidth: 600
      },
      chartOptions: {
        series: [{
          dataLabels: {
            distance: -30
          }
        }]
      }
    }]
  }
});
    </script> -->

     <script type="text/javascript">
      $(document).ready(function() {
          $("#select2insidemodal").select2({
            width: '100%', 
          });
        });
    </script>



</body>

</html>