<div id="modalTambahSurat" class="modal fade" data-width="660">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahSurat" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/add-button.png">&nbsp;&nbsp;&nbsp;REQUEST NOMOR SURAT PERUSAHAAN</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <!--<div class="col-md-12">
                    <label>Kode Item:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_kode"
                            class="form-control"
                            placeholder="Ex : BRG00221"
                            required>
                    </p>
                   
                </div>-->
                <div class="col-md-12">
                    <label>PERIHAL SURAT:</label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="surat_hal"
                            class="form-control"
                            placeholder="Ex : Permohonan PO"
                            required>
                    </p>
                   <br>
                </div>
                <div class="col-md-12">
                    <label>KODE SURAT:</label>
                    <p>
                        <font size="0">PETUNJUK : [KD PERUSAHAAN] / [KETERANGAN]</font>
                        <input
                            style="color: black;"
                            type="text"
                            name="kode_string"
                            class="form-control"
                            placeholder="Ex : ILHAM/PO"
                            required>
                        <font size="0">KET : <b>ILHAM</b> -> Sebagai kode Perusahaan client, <b>PO</b> -> Sebagai tujuan surat untuk keperluan PO (Purchase Order)<br>PREVIEW : 001/GPB-<b>ILHAM/PO</b>/II/2022</font>
                    </p>
                    <br>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button style="width:100%" type="submit" class="btn btn-blue">
                <b>BOOKING NOMOR SURAT</b>
            </button>
        </div>
    </form>
</div>