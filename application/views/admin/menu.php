<ul class="main-navigation-menu">
    
    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">BERANDA</font></b>
        </a>
    </li>  
    <li>
        <a 
            <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin?home=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['home']) && $_GET['home']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">DASHBOARD</span>
        </a>
    </li>
    

    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">DATA LOGISTIK</font></b>
        </a>
    </li> 

    <li>
        <a 
            <?php if (isset($_GET['kirim']) && $_GET['kirim']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_pengiriman?kirim=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['kirim']) && $_GET['kirim']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">PENGIRIMAN LOGISTIK</span> 
        </a>
    </li>   

    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">SCANNING QR-CODE</font></b>
        </a>
    </li> 
    <li>
        <a 
            target="_blank" <?php if (isset($_GET['scan']) && $_GET['scan']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_scan_logistik?scan=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['scan']) && $_GET['scan']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">CHECKPOINT</span> 
        </a>
    </li>  
    <li>
        <a 
            target="_blank" <?php if (isset($_GET['bast']) && $_GET['bast']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_bast?bast=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['bast']) && $_GET['bast']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">SERAH TERIMA</span> 
        </a>
    </li>

    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">DATABASE PERSURATAN</font></b>
        </a>
    </li>  
    <li>
        <a 
            <?php if (isset($_GET['surat']) && $_GET['surat']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_surat?surat=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['surat']) && $_GET['surat']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">PENOMORAN SURAT</span> 
        </a>
    </li>  

    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">TAGIHAN SERVER</font></b>
        </a>
    </li>  
    <li>
        <a 
            <?php if (isset($_GET['inv']) && $_GET['inv']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_invoice?inv=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['inv']) && $_GET['inv']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">INVOICE</span> 
        </a>
    </li>  

    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">E-MAIL PERUSAHAAN</font></b>
        </a>
    </li>  
    <li>
        <a 
            href="https://gawaipeladen.com/webmail" target="_blank">
            <i class="fa fa-square-o" <?php if (isset($_GET['inv']) && $_GET['inv']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">LOGIN WEBMAIL</span> 
        </a>
    </li> 

    <li style="background-color: #ddecf6; pointer-events: none;">
        <a>
            <b><font color="black">DATA GENERAL</font></b>
        </a>
    </li> 
    <li>
        <a 
            target="_blank" 
            href="https://siplah.blibli.com/merchant-detail/SPGP-0016?itemPerPage=40&page=0&merchantId=SPGP-0016">
            <i class="fa fa-square-o" <?php if (isset($_GET['admwil']) && $_GET['admwil']==1){echo 'style="background: ; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">SIPLAH BLIBLI STORE</span> 
        </a>
    </li> 
    <li>
        <a 
            target="_blank" 
            href="<?php echo base_url() ?>index.php/frontend">
            <i class="fa fa-square-o" <?php if (isset($_GET['admwil']) && $_GET['admwil']==1){echo 'style="background: ; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">LANDING PAGE FRONT</span> 
        </a>
    </li>  
    <li>
        <a 
            <?php if (isset($_GET['admwil']) && $_GET['admwil']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url();?>index.php/admin/bg_adminwilayah?admwil=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['admwil']) && $_GET['admwil']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">ADMIN WILAYAH</span> 
        </a>
    </li>
    <li>
        <a 
            <?php if (isset($_GET['pwd']) && $_GET['pwd']==1){echo 'style="background: #DCDCDC; color: black;"';}?> 
            href="<?php echo base_url() ?>index.php/admin/bg_password?pwd=1">
            <i class="fa fa-square-o" <?php if (isset($_GET['pwd']) && $_GET['pwd']==1){echo 'style="background: #DCDCDC; color: black;"';} else { echo 'style="color: black;"';}?> ></i>
            <span class="title">GANTI PASSWORD</span> 
        </a>
    </li>  
    <li>
        <a
            href="<?php echo base_url();?>index.php/welcome/logout/">
            <i class="fa fa-square-o" style="color: black;" ></i>
            <span class="title">LOGOUT</span>
        </a>
    </li>
    
       
</ul>