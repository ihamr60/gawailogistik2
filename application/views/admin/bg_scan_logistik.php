<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title>CHECKPOINT <?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url();?>vendor/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" />

    <!--  BUTTON -->
    <link href="<?php echo base_url();?>vendor/bower_components/ladda-bootstrap/dist/ladda-themeless.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>vendor/bower_components/bootstrap-social/bootstrap-social.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
</head>

<body class="lock-screen">

    <div class="main-ls">
        <div class="logo">
            <img width="7%" src="<?php echo base_url() ?>vendor/assets/images/web/marker.png"> - <b><?php echo $wilayah ?></b>
        </div>
        <div class="box-ls">
            <img alt="" width="34%" src="<?php echo base_url() ?>vendor/assets/images/web/fingerprint-scanner.png" />
            <div class="user-info">
                <h1><i class="fa fa-lock"></i> Scanning Logistik</h1>
                <span>Checkpoint PT. Gawai Peladen Barokah</span>
                <span><em>Please enter or scanning your SPK number.</em></span>
                <form method="post" action="<?php echo base_url() ?>index.php/admin/scan_logistik">
                    <div class="input-group">
                        <input type="hidden" value="<?php echo $wilayah ?>" name="trace_lokasi">
                        <input type="text" name="trace_resi" placeholder="SPK Number" class="form-control" autofocus>
                        <span class="input-group-btn">
                            <button class="btn btn-blue" type="submit">
                                <i class="fa fa-chevron-right"></i>
                            </button>
                        </span>
                    </div>
                    <br>
                    <div class="input-group">
                        <input type="hidden" name="trace_ket" value="Moda dalam perjalanan (sedang melewati checkpoint / pemeriksaan wilayah <b><?php echo $wilayah ?></b>)" class="form-control" autofocus>
                        
                    </div>
                    <div class="relogin">
                            <?php //echo $this->session->flashdata('info'); ?>
                    </div>
                </form>
            </div>
            <br><br>
            <?php echo $this->session->flashdata('info'); ?>
        </div>
        <div class="copyright">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
    </div>



    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
             <script src="../../bower_components/respond/dest/respond.min.js"></script>
             <script src="../../bower_components/Flot/excanvas.min.js"></script>
             <script src="../../bower_components/jquery-1.x/dist/jquery.min.js"></script>
             <![endif]-->

    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>vendor/assets/js/min/main.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

     <?php echo $this->session->flashdata('info2'); ?>

    <script>
        jQuery(document).ready(function() {
            Main.init();
        });
    </script>

</body>

</html>