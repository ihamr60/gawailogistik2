<div id="modalTambahLogistik" class="modal fade" data-width="860">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahLogistik" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/add-button.png">&nbsp;&nbsp;&nbsp;TAMBAH LOGISTIK</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <label><b>NO. SPK :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_kode"
                            class="form-control"
                            placeholder="Ex : S10002267825"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-6">
                    <label><b>NO. RESI INTERNAL :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_internal"
                            class="form-control"
                            value="-"
                            placeholder="Ex : GP100022675 (- jika kosong)"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label><b>KOTA TUJUAN :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_tujuan"
                            class="form-control"
                            placeholder="Ex: Kota Medan"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label><b>JENIS KENDARAAN :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_kendaraan"
                            class="form-control"
                            placeholder="Ex : MOBIL BOX"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label><b>NO. POLISI :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_no_polisi"
                            class="form-control"
                            placeholder="Ex : BK 1234 FU"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label><b>LOKASI MUAT :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_lokasi_muat"
                            class="form-control"
                            placeholder="Ex : PT. GAWAI PELADEN BAROKAH"
                            value="PT. GAWAI PELADEN BAROKAH"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label><b>ALAMAT MUAT - I :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_alamat_muat1"
                            class="form-control"
                            placeholder="Ex : Jl. Bhayangkara Komplek Alam Sutra, Pakualam"
                            value="Jl. Bhayangkara Komplek Alam Sutra, Pakualam"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label><b>ALAMAT MUAT - II :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_alamat_muat2"
                            class="form-control"
                            placeholder="Ex : Serpong Utara, Kota Tangerang Selatan, Banten 15325"
                            value="Serpong Utara, Kota Tangerang Selatan, Banten 15325"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label><b>LOKASI BONGKAR :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_lokasi_bongkar"
                            class="form-control"
                            placeholder="Ex : SMKS MUHAMMADIYAH 5 BABAT"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label><b>ALAMAT BONGKAR - I :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_alamat_bongkar1"
                            class="form-control"
                            placeholder="Ex : Jl. Rumah Sakit, No. 15-17"
                            required>
                    </p>
                </div>
                <div class="col-md-6">
                    <label><b>ALAMAT BONGKAR - II :</b></label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="resi_alamat_bongkar2"
                            class="form-control"
                            placeholder="Ex : Tanggul Rejo, Babat"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button style="width:100%" type="submit" class="btn btn-blue">
                <b>SUBMIT</b>
            </button>
        </div>
    </form>
</div>