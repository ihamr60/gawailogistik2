<?php
    function rupiah2($angka){
    
    $hasil_rupiah = "" . number_format($angka,0,',','.');
    return $hasil_rupiah;
 
}
?>
<div id="modalBayar" class="modal fade" tabindex="-1" data-width="560" style="display: none;">
    <form role="form" action="<?php echo base_url();?>index.php/admin/bayar" method="post" name="autoSumForm">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/icon/hospital.png">&nbsp;&nbsp;&nbsp;BAYAR CASH</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <label>TOTAL BELANJA:</label>
                    <p>
                        <input
                            type="text"
                            name="total_belanja"
                            style="font-size:25px;"
                            class="form-control"
                            value="<?php echo rupiah2($total_belanja['sum']) ?>"
                            required
                            readonly
                            onFocus="startCalc();" onBlur="stopCalc();">
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>TOTAL DIBAYAR:</label>
                    <p>
                        <input
                            type="text"
                            name="dibayar"
                            style="font-size:25px;"
                            class="form-control uang"
                            placeholder="0"
                            required
                            onFocus="startCalc();" onBlur="stopCalc();">
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>KEMBALIAN:</label>
                    <p>
                        <input
                            type="text"
                            name="kembali"
                            style="font-size:25px;"
                            class="form-control uang"
                            value="0"
                            readonly
                            required
                            onFocus="startCalc();" onBlur="stopCalc();">
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                CANCEL
            </button>
            <button type="submit" class="btn btn-green">
                BAYAR
            </button>
        </div>
    </form>
</div>

<script>


    function startCalc()
    {
        interval = setInterval("calc()",1);
    }

    function calc()
    {
        var one_1 = document.autoSumForm.total_belanja.value;
        var two_1 = document.autoSumForm.dibayar.value;

        var regex = /[.,\sRp]/g;
        one = one_1.replace(regex, '');
        two = two_1.replace(regex, '');

        var kembalian = (two * 1) - (one * 1);

         var number_string = kembalian.toString(),
        sisa    = number_string.length % 3,
        rupiah  = number_string.substr(0, sisa),
        ribuan  = number_string.substr(sisa).match(/\d{3}/g);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        if(kembalian > 0)
        {
            document.autoSumForm.kembali.value = (rupiah);
        }
        else if(two_1 == one_1)
        {
            document.autoSumForm.kembali.value = 'Tidak ada kembalian (pass)';
        }
        else
        {
            document.autoSumForm.kembali.value = 'Minus (-)';
        }
    }

    function stopCalc()
    {
        clearInterval(interval);
    }
</script>