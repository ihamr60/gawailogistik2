<div id="modalTambahBarang" class="modal fade" data-width="360">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahBarang/<?php echo $this->uri->segment(3) ?>" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/add-button.png">&nbsp;&nbsp;&nbsp;TAMBAH BARANG</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <!--<div class="col-md-12">
                    <label>Kode Item:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_kode"
                            class="form-control"
                            placeholder="Ex : BRG00221"
                            required>
                    </p>
                   
                </div>-->
                <div class="col-md-12">
                    <label>Nama Item / Barang:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_nama"
                            class="form-control"
                            placeholder="Ex : Papan Tulis Whiteboard"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>Qty:</label>
                    <p>
                        <input
                            type="number"
                            name="barang_qty"
                            class="form-control"
                            placeholder="Ex: 1"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Satuan:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_satuan"
                            class="form-control"
                            placeholder="Ex: Unit"
                            required>
                    </p>
                </div>
                <div class="col-md-12">
                    <label>Total Harga:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_hg_total"
                            class="form-control uang"
                            placeholder="Ex: 1000000"
                            required>
                    </p>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button style="width:100%" type="submit" class="btn btn-blue">
                <b>SUBMIT</b>
            </button>
        </div>
    </form>
</div>