<div id="modalTambahUser" class="modal fade" data-width="460">
    <form role="form" action="<?php echo base_url();?>index.php/admin/tambahUser" method="post">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                &times;
            </button>

            <h4 class="modal-title"> <img width="30px" src="<?php echo base_url()?>vendor/assets/images/web/add-button.png">&nbsp;&nbsp;&nbsp;TAMBAH AKUN PENGGUNA</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <!--<div class="col-md-12">
                    <label>Kode Item:</label>
                    <p>
                        <input
                            type="text"
                            name="barang_kode"
                            class="form-control"
                            placeholder="Ex : BRG00221"
                            required>
                    </p>
                   
                </div>-->
                <div class="col-md-12">
                    <label>USERNAME:</label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="user_username"
                            class="form-control"
                            placeholder="Ex : admin1383"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>NAMA LENGKAP:</label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="user_nama"
                            class="form-control"
                            placeholder="Ex : Ilham Ramadhan, S. Tr. Kom"
                            required>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>LEVEL AKSES:</label>
                    <p>
                        <select
                            style="color: black;"
                            name="user_role"
                            class="form-control"
                            required>
                            <option value="">Please Select</option>
                            <option value="1">Super Admin</option>
                            <option value="2">Admin Wilayah</option>
                        </select>
                    </p>
                   
                </div>
                <div class="col-md-12">
                    <label>WILAYAH:</label>
                    <p>
                        <input
                            style="color: black;"
                            type="text"
                            name="user_wilayah"
                            class="form-control"
                            placeholder="Ex : Kota Langsa"
                            required>
                    </p>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button style="width:100%" type="submit" class="btn btn-blue">
                <b>SUBMIT</b>
            </button>
        </div>
    </form>
</div>