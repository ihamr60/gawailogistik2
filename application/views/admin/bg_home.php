<?php
    function rupiah($angka){
    
    $hasil_rupiah = "<b>Rp.</b> " . number_format($angka,0,',','.');
    return $hasil_rupiah;
 
}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo $data_config['nama_app'] ?></title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>upload/icon/<?php echo $data_config['icon_app'] ?>" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="<?php echo $data_config['nama_app'] ?>" name="description" />
    <meta content="<?php echo $data_config['nama_app'] ?>" name="multidatanesia" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/font-awesome/css/font-awesome.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/fonts/clip-font.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/iCheck/skins/all.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main.min.css" />
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>vendor/assets/css/main-responsive.min.css" />
    <link type="text/css" rel="stylesheet" media="print" href="<?php echo base_url(); ?>vendor/assets/css/print.min.css" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="<?php echo base_url(); ?>vendor/assets/css/theme/light.min.css" />
    <?php $this->load->view('css') ?>

    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="<?php echo base_url(); ?>vendor/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/summernote/dist/summernote.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>vendor/bower_components/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" />
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';
    </style>

    <!-- DATA GRAFIK -->
    <style type="text/css">
        @import 'https://code.highcharts.com/css/highcharts.css';

        .highcharts-figure,
        .highcharts-data-table table {
          min-width: 100%;
          max-width: 100%;
          margin: 1em auto;
        }

        .highcharts-data-table table {
          font-family: Verdana, sans;
          border-collapse: collapse;
          border: 1px solid #ebebeb;
          margin: 10px auto;
          text-align: center;
          width: 100%;
          max-width: 500px;
        }

        .highcharts-data-table caption {
          padding: 1em 0;
          font-size: 1.2em;
          color: #555;
        }

        .highcharts-data-table th {
          font-weight: 600;
          padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
          padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
          background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
          background: #f1f7ff;
        }
    </style>


</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        	<?php echo $atas; ?>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <!-- end: HEADER -->
        <br>
    <!-- start: MAIN CONTAINER -->
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <?php echo $menu; ?>
                <!-- end: MAIN NAVIGATION MENU -->
            </div>
            <!-- end: SIDEBAR -->
        </div>

        <!-- start: PAGE -->
        <div class="main-content">
           
            <div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row hidden-xs hidden-sm">
                    <div class="col-sm-12">
                        
                        <!-- start: PAGE TITLE & BREADCRUMB -->
                        <ol class="breadcrumb">
                            <li class="active">
                                    Home
                            </li>
                        </ol>
                        <div class="page-header">
                            <?php echo $bio; ?>
                        </div>
                        <!-- end: PAGE TITLE & BREADCRUMB -->
                    </div>
                </div>
                <!-- end: PAGE HEADER -->
                <!-- start: PAGE CONTENT -->
                <?php echo $this->session->flashdata('info'); ?>
                <?php 
                    if(date('d') == 1)
                    {
                        echo '<div class="alert alert-warning hidden-xs hidden-sm">
                                <button data-dismiss="alert" class="close">
                                    &times;
                                </button>
                              <font color="black">
                               <strong> REMINDER !!! -</strong> Waktunya <b>UPDATE HARGA di STORE SIPLAH BLIBLI</b> setiap tanggal 2 pada bulan berjalan!</font>
                            </div> ';
                    }
                ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <form role="form" action="<?php echo base_url();?>index.php/admin/addtocart" method="post">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <a style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        background-color: white;
                                                        border: 10px dash grey;" 
                                                        class="btn btn-icon btn-block">
                                                        <h3><?php echo rupiah($target_penerimaan['target_penerimaan']) ?></h3>
                                                        <span class="badge badge-teal"
                                                        style="
                                                            box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                            padding: 10px; 
                                                            border: 1px grey;">
                                                        TARGET <b>YANG HARUS DITERIMA</b></span>
                                                    </a>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        border: 10px dash grey;
                                                        background-color: white;" 
                                                        class="btn btn-icon btn-block">
                                                        <h3><?php echo rupiah($penerimaan_saat_ini['penerimaan_saat_ini']) ?></h3>
                                                        <span class="badge badge-teal" 
                                                        style="
                                                            box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                            padding: 10px; 
                                                            border: 1px grey;" >
                                                        <font color="white">TOTAL <b>TERTAGIH SAAT INI</b></font></span>
                                                    </a>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        background-color: white;
                                                        border: 10px dash grey;" 
                                                        class="btn btn-icon btn-block">
                                                        <h3><?php echo rupiah($belum_diterima['belum_diterima']) ?></h3>
                                                        <span class="badge badge-teal"
                                                        style="
                                                            box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                            padding: 10px; 
                                                            border: 1px grey;">
                                                        TOTAL <b>YANG BELUM TERTAGIH</b></span>
                                                    </a>
                                                </div>
                                                <div class="col-sm-3">
                                                    <a style="
                                                        box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                        padding: 10px; 
                                                        background-color: white;
                                                        border: 10px dash grey;" 
                                                        class="btn btn-icon btn-block">
                                                        <h3><?php echo $menunggu_invoice['menunggu_invoice'] ?> SPK Logistik</h3>
                                                        <span class="badge badge-teal"
                                                        style="
                                                            box-shadow: 2px 2px 2px rgba(0,0,0,0.8); 
                                                            padding: 10px; 
                                                            border: 1px grey;">
                                                        MENUNGGU <b>UNGGAHAN INVOICE</b></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                  <div class="col-sm-7">
                        <div class="panel panel-default">
                            
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="grafik1"></div>
                                  
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="panel panel-default">
                            
                            <div class="panel-body">
                                <figure class="highcharts-figure">
                                  <div id="grafik2"></div>
                                  
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
    </div>
    <!-- end: MAIN CONTAINER -->

    <!-- start: FOOTER -->
    <div class="footer clearfix">
        <div class="footer-inner">
            <script>
                document.write(new Date().getFullYear())
            </script> &copy; <?php echo $data_config['versi'] ?>
        </div>
        <div class="footer-items">
            <span class="go-top"><i class="clip-chevron-up"></i></span>
        </div>
    </div>
    <!-- end: FOOTER -->
 
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>vendor/bower_components/respond/dest/respond.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/Flot/excanvas.min.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bower_components/jquery-1.x/dist/jquery.min.js"></script>
        <![endif]-->
    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->

    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>vendor/assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/autosize/dist/autosize.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/summernote/dist/summernote.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
    <script src="<?php echo base_url();?>vendor/bower_components/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="<?php echo base_url();?>vendor/assets/js/min/form-elements.min.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <?php $this->load->view('admin/menu_bawah'); ?>
    <?php echo $this->session->flashdata('info2'); ?>
    
     <script>
        jQuery(document).ready(function() {
            Main.init();
        });
    </script>

    <script type="text/javascript">
      Highcharts.chart('grafik1', {

  title: {
    text: '<b>GRAFIK TREN ORDER PENGIRIMAN BARANG LOGISTIK</b>',
    style: {
        fontFamily: 'Poppins, poppins'
      }
  },

  subtitle: {
    text: 'Source: PT Gawai Peladen Barokah',
    style: {
        fontFamily: 'Poppins, poppins'
      }
  },

  yAxis: {
    title: {
      text: 'Jumlah Pengiriman Logistik',
      style: {
        fontFamily: 'Poppins, poppins'
      }
    }
  },

  xAxis: {
    accessibility: {
      rangeDescription: '-',
      style: {
        fontFamily: 'Poppins, poppins'
      }
    }
  },

  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'middle',
  },

  plotOptions: {
    series: {
      label: {
        connectorAllowed: false,
        style: {
        fontFamily: 'Poppins, poppins'
      }
      },
      pointStart: 1
    }
  },

  series: [
          {
            name: 'Jumlah Pengiriman',
            color: '#2F4F4F',
            data: [
            <?php
              foreach($data_grafik1->result_array () as $d)
              {
                echo "['".date('d-F-Y', strtotime($d['resi_created_at']))."',".$d['total']."],";
              }
            ?>
        ]
          }],

  responsive: {
    rules: [{
      condition: {
        maxWidth: 500
      },
      chartOptions: {
        legend: {
          layout: 'horizontal',
          align: 'center',
          verticalAlign: 'bottom',
        },
      }
    }]
  }

});
    </script>


    <script type="text/javascript">
      Highcharts.chart('grafik2', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'TOTAL CHECKPOINT LOGISTIK <br><b>DALAM PERJALANAN</b>',
    style: {
        fontFamily: 'Poppins, poppins'
      }
  },
  subtitle: {
    text: 'Source: PT GAWAI PELADEN BAROKAH</a>',
    style: {
        fontFamily: 'Poppins, poppins'
      }
  },
  xAxis: {
    type: 'category',
    labels: {
      rotation: -45,
      style: {
        fontSize: '13px',
        fontFamily: 'Poppins, poppins'
      }
    }
  },
  yAxis: {
    min: 0,
    title: {
      text: 'Estimasi total transit (Checkpoint)',
      style: {
        fontSize: '13px',
        fontFamily: 'Poppins, poppins'
      }
    }
  },
  legend: {
    enabled: false
  },
  tooltip: {
    pointFormat: 'Total Transit: <b>{point.y:.0f} x Checkpoint</b>'
  },

  plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        location.href = '<?php echo base_url() ?>index.php/admin/bg_pengiriman/' +
                            this.options.key;
                    }
                }
            }
        }
    },


  series: [{
    name: 'SPK Logistik',
    color: '#5F9EA0',
    data: [
    <?php 
      foreach($data_grafik2->result_array() as $d)
      {
        echo "
            {
            y: ".$d['total'].",
            name: '".$d['trace_resi']."<br>".$d['resi_tujuan']."',
            key: '".$d['trace_resi']."?kirim=1'
        },";
      }
    ?>
      
    ],
    dataLabels: {
      enabled: true,
      rotation: 0,
      color: '#2F4F4F',
      align: 'center',
      format: '{point.y:.0f} x Checkpoint', // one decimal
      y: 10, // 10 pixels down from the top
      style: {
        fontSize: '13px',
        fontFamily: 'Poppins, poppins'
      }
    }
  }]
});
    </script>

</body>

</html>