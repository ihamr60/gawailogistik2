<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * IT PDAM Kota langsa
	 * HP : 0853 6188 5100
	 * Email: ilhamr6000@gmail.com
	 */

	private $db2;

	 public function __construct()
	 {
	  parent::__construct();
	         $this->db2 = $this->load->database('db2', TRUE);
	 }

	public function test_print()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada



			
			$bc['data_barang']		= $this->web_app_model->getAllData('tbl_barang');
			$bc['total_belanja']	= $this->web_app_model->getSUMAllData('tbl_cart','cart_jumlah','cart_paid','0');
			$bc['total_produk']		= $this->web_app_model->getCOUNTAllData('tbl_cart','cart_jumlah','cart_paid','0');
			//$bc['data_cart']		= $this->web_app_model->getAll2Join('cart_item','barang_no','tbl_cart','tbl_barang');

			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$bc['modalBayar'] 			= $this->load->view('admin/modalBayar',$bc,true);	
			$this->load->view('test_print',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function index()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada


			$bc['target_penerimaan'] 	= $this->web_app_model->target_penerimaan();
			$bc['penerimaan_saat_ini'] 	= $this->web_app_model->penerimaan_saat_ini();
			$bc['belum_diterima'] 		= $this->web_app_model->belum_diterima();
			$bc['menunggu_invoice'] 	= $this->web_app_model->menunggu_invoice();

			$bc['data_grafik1']		= $this->web_app_model->getgrafik1();
			$bc['data_grafik2']		= $this->web_app_model->getgrafik2();

			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_home',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}	





	// START INVOICE SERVER

	public function bg_invoice()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	

			//$bc['maxKode']   		= $this->web_app_model->maxKodeSurat(date('m'),date('Y'));

			//$bc['modalTambahSurat'] = $this->load->view('admin/modalTambahSurat',$bc,true);

			$this->load->view('admin/bg_invoice',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_upload_pembayaran()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada


			$bc['data_invoice']		= $this->web_app_model->getWhereOneItem_db2('GPB','inv_client','tbl_invoice');

			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	

			//$bc['maxKode']   		= $this->web_app_model->maxKodeSurat(date('m'),date('Y'));

			//$bc['modalTambahSurat'] = $this->load->view('admin/modalTambahSurat',$bc,true);

			$this->load->view('admin/bg_upload_pembayaran',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function upload_pembayaran()
	{
		$inv_kdinvoice				= $this->input->post('inv_kdinvoice');

		// get foto
		 $config['upload_path'] 	= './upload/kwitansi_invoice';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= "KWITANSI-".$inv_kdinvoice;

     	 $this->load->library('upload', $config);


			if(!empty($_FILES['inv_kwitansi']['name'])) 
			{
		        if ( $this->upload->do_upload('inv_kwitansi') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'inv_stts' 			=> "2",
						'inv_kwitansi' 		=> $foto['file_name'],
						);

					$where = array(
						'inv_kdinvoice'		=> $inv_kdinvoice,
					);

		
					$this->web_app_model->updateDataWhere_db2($where, $data,'tbl_invoice');
					header('location:'.base_url().'index.php/admin/bg_invoice?inv=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
															<font color='black'>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Invoice berhasil diupdate!
															</font>
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Invoice berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/admin/bg_invoice?inv=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan File max 10 Mb atau hubungi Konsultan IT',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/admin/bg_invoice?inv=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'File kosong!',
									                text:  'Mohon lampirkan file Invoice',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	function data_invoice(){

        $draw 		= $this->input->post('draw');
       	

        if($this->input->post('search')['value'] == '')
        {
        	$search 	= $this->uri->segment(3);
        }
        else
        {
        	$search 	= $this->input->post('search')['value'];
        }

        
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db2->from('tbl_invoice')
				->where('inv_client','GPB');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db2->order_by('inv_no', $value['dir']);
						break;
					case 1:
						$this->db2->order_by('inv_kdinvoice', $value['dir']);
						break;
					case 2:
						$this->db2->order_by('inv_client', $value['dir']);
						break;
					case 3:
						$this->db2->order_by('inv_periode', $value['dir']);
						break;
					case 4:
						$this->db2->order_by('inv_nominal', $value['dir']);
						break;
					case 5:
						$this->db2->order_by('inv_surat', $value['dir']);
						break;
					case 6:
						$this->db2->order_by('inv_stts', $value['dir']);
						break;
					case 7:
						$this->db2->order_by('inv_kwitansi', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db2->like("inv_kdinvoice",$search);
			$this->db2->or_like("inv_client",$search);
		}

		$tempdb = clone $this->db2;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db2->limit($length,$start);

		$data = $this->db2->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
		
			if($d['inv_stts'] == 1)
			{
				$stts 			= "<span style='text-transform: uppercase;' class='label label-success'>LUNAS</span>";
				$action 		= "LUNAS";
			}
			else if($d['inv_stts'] == 0)
			{
				$stts = "<span style='text-transform: uppercase;' class='label label-danger'>BELUM LUNAS</span>";
				$action = "<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_upload_pembayaran?inv=1'>
                                <i class='fa fa-send-o'></i> Upload Bukti Pembayaran
                            </a>
                        </li>
                    </ul>
                </div>";
			}
			else if($d['inv_stts'] == 2)
			{
				$stts = "<span style='text-transform: uppercase;' class='label label-yellow'><font color='black'>MENUNGGU VERIFIKASI</font></span>";
				$action = "<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_upload_pembayaran?inv=1'>
                                <i class='fa fa-send-o'></i> Upload Bukti Pembayaran
                            </a>
                        </li>
                    </ul>
                </div>";
			}

			if($d['inv_kwitansi'] != "")
			{
				$kwitansi 		= "<a href='https://gawaipeladen.com/upload/kwitansi_invoice/".$d['inv_kwitansi']."'><font color='green'>".$d['inv_kwitansi']."</font></a>";
			}
			else if($d['inv_kwitansi'] == "")
			{
				$kwitansi 		= "Tidak tersedia";
			}

			$hapus 					= '"Are you sure you want to delete the '.$d['inv_kdinvoice'].'?"';

			$output['data'][]=array(
				$no,
				$d['inv_kdinvoice'],
				$d['inv_client'],
				$d['inv_periode'],
				rupiah($d['inv_nominal']),
				"<a href='https://acehdev.web.id/hosting/upload/invoice/".$d['inv_surat']."'><font color='green'>".$d['inv_surat']."</font></a>",
				$stts,
				$kwitansi,
				$action);
			$no++;
		}

		$output['recordsTotal'] = $this->db2->from('tbl_invoice')
											->where('inv_client','GPB')
											->get()->num_rows();

		echo json_encode($output);
    } 

	// END INVOICE SERVER





	// START MANAJEMEN SURAT 

	public function bg_surat()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	

			//$bc['maxKode']   		= $this->web_app_model->maxKodeSurat(date('m'),date('Y'));

			$bc['modalTambahSurat'] = $this->load->view('admin/modalTambahSurat',$bc,true);

			$this->load->view('admin/bg_surat',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_upload_surat()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_surat']		= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'surat_no','tbl_persuratan');

			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	

			$this->load->view('admin/bg_upload_surat',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function upload_surat()
	{
		$surat_no				= $this->input->post('surat_no');
		$surat_string			= $this->input->post('surat_string');

		// get foto
		 $config['upload_path'] 	= './upload/arsip_surat';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $surat_string;

     	 $this->load->library('upload', $config);


			if(!empty($_FILES['surat_lampiran']['name'])) 
			{
		        if ( $this->upload->do_upload('surat_lampiran') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'surat_lampiran' 		=> $foto['file_name'],
						);

					$where = array(
						'surat_no'				=> $surat_no,
					);

		
					$this->web_app_model->updateDataWhere($where, $data,'tbl_persuratan');
					header('location:'.base_url().'index.php/admin/bg_surat?kirim=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
															<font color='black'>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Data Arsip Surat berhasil diupdate!
															</font>
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data Arsip Surat berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/admin/bg_surat?kirim=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan File max 10 Mb atau hubungi Konsultan IT',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/admin/bg_surat?kirim=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'File kosong!',
									                text:  'Mohon lampirkan file Surat',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	function data_surat(){

        $draw 		= $this->input->post('draw');
       	

        if($this->input->post('search')['value'] == '')
        {
        	$search 	= $this->uri->segment(3);
        }
        else
        {
        	$search 	= $this->input->post('search')['value'];
        }

        
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_persuratan')
				->join('tbl_user', 'tbl_persuratan.surat_user = tbl_user.user_username');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('surat_no', $value['dir']);
						break;
					case 1:
						$this->db->order_by('surat_no', $value['dir']);
						break;
					case 2:
						$this->db->order_by('surat_hal', $value['dir']);
						break;
					case 3:
						$this->db->order_by('surat_date', $value['dir']);
						break;
					case 4:
						$this->db->order_by('surat_lampiran', $value['dir']);
						break;
					case 5:
						$this->db->order_by('surat_status', $value['dir']);
						break;
					case 6:
						$this->db->order_by('surat_user', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("surat_string",$search);
			$this->db->or_like("surat_hal",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
	
			$hapus 					= '"Are you sure you want to delete the '.$d['surat_string'].'?"';

			if($d['surat_lampiran'] == '')
			{
				$lampiran   = "
								<p align='center'>
									<a onclick='return false;' href='#'>
										<img width='25px' src='".base_url()."vendor/assets/images/web/forbidden.png'>
									</a>
								</p>";
				$status  	= "<span style='text-transform: uppercase;' class='label label-yellow'><font color='black'>Menunggu Upload</font></span>";
			}
			else
			{
				$lampiran   = "
								<p align='center'>
									<a href='".base_url()."upload/arsip_surat/".$d['surat_lampiran']."'>
										<img width='25px' src='".base_url()."vendor/assets/images/web/google-docs.png'>
									</a>
								</p>";
				$status  	= "<span style='text-transform: uppercase;' class='label label-primary'><font color='white'>Surat-diarsipkan</font></span>";
			}
			
				

			$output['data'][]=array(
				"<p align='center'>".$no."</p>",
				"<p style='text-transform: uppercase;'>".$d['surat_string']."</p>",
				"<p style='text-transform: uppercase;'>".$d['surat_hal']."</p>",
				"<p style='text-transform: uppercase;' align='center'>".date('d M Y', strtotime($d['surat_date']))."</p>",
				$lampiran,
				"<p align='center'>".$status."</p>",
				"<p style='text-transform: uppercase;' align='center'>".$d['user_nama']."</p>",
				"<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_upload_surat/".$d['surat_no']."?surat=1'>
                                <i class='clip-file-2'></i> Upload Arsip
                            </a>
                        </li>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' onclick='return confirm(".$hapus.")' href='".base_url()."index.php/admin/hapus_surat/".$d['surat_no']."/".$d['surat_lampiran']."?surat=1'>
                                <i class='fa fa-trash-o'></i> Hapus Surat
                            </a>
                        </li>
                    </ul>
                </div>");
			$no++;
		}

		$output['recordsTotal'] = $this->db->from('tbl_persuratan')
											->join('tbl_user', 'tbl_persuratan.surat_user = tbl_user.user_username')
											->get()->num_rows();

		echo json_encode($output);
    } 

    public function hapus_surat()
	{
		$surat_no					= $this->uri->segment(3);
		$surat_lampiran				= $this->uri->segment(4); 
		$hapus_tbl_persuratan 		= array('surat_no'=>$surat_no);

	  	$path_surat 				= './upload/arsip_surat/';
	  	@unlink($path_surat.$surat_lampiran);

		$this->web_app_model->deleteData('tbl_persuratan',$hapus_tbl_persuratan);
		header('location:'.base_url().'index.php/admin/bg_surat?surat=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Arsip has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Arsip has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}


    public function tambahSurat()
	{
		$maxKode   			= $this->web_app_model->maxKodeSurat(date('m'),date('Y'));

		function getRomawi($bln){
	        switch ($bln){
	        case 1: 
	            return "I";
	        break;
	        case 2:
	            return "II";
	        break;
	        case 3:
	            return "III";
	        break;
	        case 4:
	            return "IV";
	        break;
	        case 5:
	            return "V";
	        break;
	        case 6:
	            return "VI";
	        break;
	        case 7:
	            return "VII";
	        break;
	        case 8:
	            return "VIII";
	        break;
	        case 9:
	            return "IX";
	        break;
	        case 10:
	            return "X";
	        break;
	        case 11:
	            return "XI";
	        break;
	        case 12:
	            return "XII";
	        break;
	        }
	    }

		date_default_timezone_set('Asia/Jakarta');
		$maxKodeSurat		= $maxKode['maxKode'];
		$kode_string		= $this->input->post('kode_string');

		$bulan 				= date('n');
		$romawi   		 	= getRomawi($bulan);

		$tahun 				= date('Y');
		$nomor 				= "/GPB-".$kode_string."/".$romawi."/".$tahun;

		$kode				= $maxKodeSurat + 1;

		$surat_kode 		= sprintf("%03s", $kode);

		$surat_string 		= $surat_kode.$nomor;
		$surat_hal 			= $this->input->post('surat_hal');
		$surat_date 		= date('Y-m-d');
		$surat_lampiran 	= "";
		$surat_status 		= "Menunggu Arsip";
		$surat_user  		= $this->session->userdata('username');

		$data = array(		
			'surat_kode' 		=> $surat_kode,
			'surat_string' 		=> $surat_string,
			'surat_hal'			=> $surat_hal,
			'surat_date'		=> $surat_date,
			'surat_status'		=> $surat_status,
			'surat_user'		=> $surat_user,
			);
		
		$this->web_app_model->insertData($data,'tbl_persuratan');
		header('location:'.base_url().'index.php/admin/bg_surat?surat=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															SUKSES !!! 
														</strong>
														<br>Data surat berhasil ditambahkan!</font>
													</p>
												</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'success',
										              title: 'Data surat berhasil ditambahkan!',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");	
	}

	// END MANAJEMEN SURAT



	// START BG PASSWORD

	public function bg_password()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_user']		= $this->web_app_model->getWhereOneItem($this->session->userdata('username'),'user_username','tbl_user');

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalTambahUser']  = $this->load->view('admin/modalTambahUser',$bc,true);
			$this->load->view('admin/bg_password',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function update_password()
	{		
		$user_username		= $this->input->post('user_username');
		$user_pwd_1			= $this->input->post('user_pwd_1');
		$user_pwd_2			= $this->input->post('user_pwd_2');

		if($user_pwd_1 == $user_pwd_2)
		{
			$data = array(		
				'user_pwd' 			=> md5($user_pwd_1),
			);

			$where = array(		
				'user_username' 	=> $user_username,
				);
			
			$this->web_app_model->updateDataWhere($where,$data,'tbl_user');
			header('location:'.base_url().'index.php/admin/bg_password?pwd=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Password berhasil diupdate !
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Success!!',
												                text:  'Password berhasil diupdate !',
											                type: 'success',
											                timer: 1000,
											                showConfirmButton: false
											            });  
											     },10);  
											    </script>
											    ");

		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_password?pwd=1');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p>
														<strong>
															<i class='icon-ok'></i>
															Maaf! - 
														</strong>
														Password tidak sama, silahkan coba lagi !
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
												     setTimeout(function () { 
												     swal({
												                title: 'Failed!!',
												                text:  'Password tidak sama, silahkan coba lagi!',
											                type: 'error',
											                timer: 1000,
											                showConfirmButton: false
											            });  
											     },10);  
											    </script>
											    ");
		}
	}

	// END BG PASSWORD



	// BG ADMIN WILAYAH START

	public function bg_adminwilayah()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$bc['modalTambahUser']  = $this->load->view('admin/modalTambahUser',$bc,true);
			$this->load->view('admin/bg_adminwilayah',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function bg_editUser()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_user']		= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'user_username','tbl_user');

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			//$bc['modalTambahUser']  = $this->load->view('admin/modalTambahUser',$bc,true);
			$this->load->view('admin/bg_editUser',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function tambahUser()
	{
		date_default_timezone_set('Asia/Jakarta');
		$user_username		= $this->input->post('user_username');
		$user_nama			= $this->input->post('user_nama');
		$user_role			= $this->input->post('user_role');
		$user_wilayah		= $this->input->post('user_wilayah');
		//$trace_updated_at	= date('Y-m-d H:i:s');

		$data = array(		
			'user_username' 	=> $user_username,
			'user_nama' 		=> $user_nama,
			'user_role'			=> $user_role,
			'user_pwd'			=> md5($user_username),
			'user_wilayah'		=> $user_wilayah,
			);
		
		$this->web_app_model->insertData($data,'tbl_user');
		header('location:'.base_url().'index.php/admin/bg_adminwilayah?admwil=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															SUKSES !!! 
														</strong>
														<br>Data user berhasil ditambahkan!</font>
													</p>
												</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'success',
										              title: 'Data user berhasil ditambahkan!',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");	
	}

	public function hapus_user()
	{
		$user_username		= $this->uri->segment(3);
		$hapus 				= array('user_username'=>$user_username);

		$this->web_app_model->deleteData('tbl_user',$hapus);
		header('location:'.base_url().'index.php/admin/bg_adminwilayah?admwil=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										User has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'User has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function edit_user()
	{		
		$user_username		= $this->input->post('user_username');
		$user_nama			= $this->input->post('user_nama');
		$user_role			= $this->input->post('user_role');
		$user_wilayah		= $this->input->post('user_wilayah');

		$data = array(		
			'user_nama' 		=> $user_nama,
			'user_role' 		=> $user_role,
			'user_wilayah' 		=> $user_wilayah,
			);

		$where = array(		
			'user_username' 	=> $user_username,
			);
		
		$this->web_app_model->updateDataWhere($where,$data,'tbl_user');
		header('location:'.base_url().'index.php/admin/bg_adminwilayah?admwil=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
												<button type='button' class='close' data-dismiss='alert'>
													<i class='icon-remove'></i>
												</button>
		
												<p>
													<strong>
														<i class='icon-ok'></i>
														Success! - 
													</strong>
													Data user berhasil diupdate !
												</p>
											</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data user berhasil diupdate !',
											                type: 'success',
											                timer: 50000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	function data_user(){

        $draw 		= $this->input->post('draw');
       	

        if($this->input->post('search')['value'] == '')
        {
        	$search 	= $this->uri->segment(3);
        }
        else
        {
        	$search 	= $this->input->post('search')['value'];
        }

        
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_user');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('user_id', $value['dir']);
						break;
					case 1:
						$this->db->order_by('user_username', $value['dir']);
						break;
					case 2:
						$this->db->order_by('user_nama', $value['dir']);
						break;
					case 3:
						$this->db->order_by('user_role', $value['dir']);
						break;
					case 4:
						$this->db->order_by('user_wilayah', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("user_nama",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
	
			$hapus 					= '"Are you sure you want to delete the '.$d['user_username'].'?"';

			if($d['user_role'] == '3')
			{
				$role   = 'User';
			}
			else if($d['user_role'] == '2')
			{
				$role   = 'Admin Wilayah';
			}
			else if($d['user_role'] == '1')
			{
				$role   = 'Super Admin';
			}
				

			$output['data'][]=array(
				$no,
				$d['user_username'],
				$d['user_nama'],
				$role,
				$d['user_wilayah'],
				"<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_editUser/".$d['user_username']."?admwil=1'>
                                <i class='clip-user'></i> Edit User
                            </a>
                        </li>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' onclick='return confirm(".$hapus.")' href='".base_url()."index.php/admin/hapus_user/".$d['user_username']."?admwil=1'>
                                <i class='fa fa-trash-o'></i> Hapus User
                            </a>
                        </li>
                    </ul>
                </div>");
			$no++;
		}

		$output['recordsTotal'] = $this->db->from('tbl_resi')
											->get()->num_rows();

		echo json_encode($output);
    } 

	// BG ADMIN WILAYAH END

	// BAST LOGISTIK - START

	public function bg_bast()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_bast',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function scan_bast()
	{
		date_default_timezone_set('Asia/Jakarta');
		$trace_resi			= $this->input->post('trace_resi');

		$cek_spk			= $this->web_app_model->getWhereOneItem($trace_resi,'resi_kode','tbl_resi');

		if(!empty($cek_spk))
		{
			header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$trace_resi.'?kirim=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															SPK ditemukan !!! 
														</strong>
														<br>Silahkan update BAST logistik Anda pada form yang tersedia disebelah kiri</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'info',
										              title: 'Silahkan upload BAST logistik !',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_bast?bast=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														SPK tidak ditemukan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'error',
										              title: 'SPK tidak ditemukan!',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");	
		}
			
	}

	public function bg_upload_bast()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada


			$bc['data_spk']			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'resi_kode','tbl_resi');

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_upload_bast',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function upload_bast()
	{
		$resi_kode				= $this->input->post('resi_kode');
		$resi_tujuan			= $this->input->post('resi_tujuan');
		$resi_stts				= $this->input->post('resi_stts');
		$resi_ket				= $this->input->post('resi_ket');

		// get foto
		 $config['upload_path'] 	= './upload/bast';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $resi_kode;

     	 $this->load->library('upload', $config);


			if(!empty($_FILES['resi_bast']['name'])) 
			{
		        if ( $this->upload->do_upload('resi_bast') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'resi_updated_at' 	=> date('Y-m-d H:i:s'),
						'resi_stts' 		=> $resi_stts,
						'resi_ket' 			=> $resi_ket,
						'resi_bast' 		=> $foto['file_name'],
						);

					$tracing = array(		
						'trace_resi' 		=> $resi_kode,
						'trace_lokasi' 		=> 'Paket telah diterima',
						'trace_ket' 		=> '<a target="_blank" href="'.base_url().'upload/bast/'.$foto['file_name'].'"><font color="#5F9EA0">Klik disini untuk melihat (Foto Serah Terima)</font></a> ',
						'trace_updated_at' 	=> date('Y-m-d H:i:s'),
						);

					$where = array(
						'resi_kode'			=> $resi_kode,
					);

		
					$this->web_app_model->updateDataWhere($where, $data,'tbl_resi');
					$this->web_app_model->insertData($tracing,'tbl_tracing');
					header('location:'.base_url().'index.php/admin/bg_pengiriman/'.$resi_kode.'?kirim=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
															<font color='black'>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Data BAST berhasil diupdate!
															</font>
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data BAST berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$resi_kode.'?kirim=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan File max 10 Mb atau hubungi IT Developer',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$resi_kode.'?kirim=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'File kosong!',
									                text:  'Mohon lampirkan file BAST',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	public function upload_invoice()
	{
		$resi_kode				= $this->input->post('resi_kode');
		$resi_tujuan			= $this->input->post('resi_tujuan');
		$resi_nominal_invoice	= str_replace(".", "", $this->input->post('resi_nominal_invoice'));

		// get foto
		 $config['upload_path'] 	= './upload/invoice';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $resi_kode;

     	 $this->load->library('upload', $config);


			if(!empty($_FILES['resi_dok_invoice']['name'])) 
			{
		        if ( $this->upload->do_upload('resi_dok_invoice') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'resi_nominal_invoice' 		=> $resi_nominal_invoice,
						'resi_dok_invoice' 			=> $foto['file_name'],
						);

					$where = array(
						'resi_kode'			=> $resi_kode,
					);

		
					$this->web_app_model->updateDataWhere($where, $data,'tbl_resi');
					header('location:'.base_url().'index.php/admin/bg_pengiriman/'.$resi_kode.'?kirim=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
															<font color='black'>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Data INVOICE berhasil diupdate!
															</font>
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data INVOICE berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$resi_kode.'?kirim=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan File max 10 Mb atau hubungi IT Developer',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$resi_kode.'?kirim=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'File kosong!',
									                text:  'Mohon lampirkan file INVOICE',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	public function upload_kwitansi()
	{
		$resi_kode				= $this->input->post('resi_kode');
		//$resi_nominal_invoice	= str_replace(".", "", $this->input->post('resi_nominal_invoice'));

		// get foto
		 $config['upload_path'] 	= './upload/kwitansi';
		 $config['allowed_types'] 	= 'jpg|png|jpeg|pdf';
		 $config['max_size'] 		= '10000';  //10MB max
		 //$config['max_width'] 	= '4480'; // pixel
		 //$config['max_height'] 	= '4480'; // pixel
		 $config['overwrite']		= true;
	     $config['file_name'] 		= $resi_kode;

     	 $this->load->library('upload', $config);


			if(!empty($_FILES['resi_dok_kwitansi']['name'])) 
			{
		        if ( $this->upload->do_upload('resi_dok_kwitansi') ) {
		            $foto = $this->upload->data();		

					$data = array(		
						'resi_dok_kwitansi' 			=> $foto['file_name'],
						);

					$where = array(
						'resi_kode'			=> $resi_kode,
					);

		
					$this->web_app_model->updateDataWhere($where, $data,'tbl_resi');
					header('location:'.base_url().'index.php/admin/bg_pengiriman/'.$resi_kode.'?kirim=1/');
					$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
															<button type='button' class='close' data-dismiss='alert'>
																<i class='icon-remove'></i>
															</button>
					
															<p>
															<font color='black'>
																<strong>
																	<i class='icon-ok'></i>
																	Success! - 
																</strong>
																Data KWITANSI berhasil diupdate!
															</font>
															</p>
														</div>");

					$this->session->set_flashdata("info2","<script type='text/javascript'>
														     setTimeout(function () { 
														     swal({
														                title: 'Success!!',
														                text:  'Data KWITANSI berhasil diupdate!',
														                type: 'success',
														                timer: 3000,
														                showConfirmButton: true
														            });  
														     },10);  
														    </script>
														    ");
				}
				else 
				{
		          	header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$resi_kode.'?kirim=1');
					$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Maaf!',
										                text:  'Data gagal tersimpan, pastikan File max 10 Mb atau hubungi IT Developer / Konsultan IT',
										                type: 'warning',
										                timer: 300000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>");
		    	}
		    }
		    else 
			{
	          	header('location:'.base_url().'index.php/admin/bg_upload_bast/'.$resi_kode.'?kirim=1/');
				$this->session->set_flashdata("info2","<script type='text/javascript'>
									     setTimeout(function () { 
									     swal({
									                title: 'File kosong!',
									                text:  'Mohon lampirkan file KWITANSI',
									                type: 'warning',
									                timer: 300000,
									                showConfirmButton: true
									            });  
									     },10);  
									    </script>");
		    }
	}

	// BAST LOGSITIK - END



	// SCAN LOGISTIK - START

	public function bg_scan_logistik()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['nama'] 			= $this->session->userdata('nama');
			$bc['wilayah'] 			= $this->session->userdata('wilayah');
			$bc['status'] 			= $this->session->userdata('stts');
			$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_scan_logistik',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function scan_logistik()
	{
		date_default_timezone_set('Asia/Jakarta');
		$trace_resi			= $this->input->post('trace_resi');
		$trace_lokasi		= $this->input->post('trace_lokasi');
		$trace_ket			= $this->input->post('trace_ket');
		$trace_updated_at	= date('Y-m-d H:i:s');

		$cek_spk			= $this->web_app_model->getWhereOneItem($trace_resi,'resi_kode','tbl_resi');
		$cek_tracing		= $this->web_app_model->cek_scan_chekpoint_duplicate($trace_resi,$trace_lokasi);

		if(!empty($cek_spk))
		{
			if(empty($cek_tracing))
			{	
				$data = array(		
				'trace_resi' 		=> $trace_resi,
				'trace_lokasi' 		=> $trace_lokasi,
				'trace_ket'			=> $trace_ket,
				'trace_updated_at'	=> $trace_updated_at,
				);
			
				$this->web_app_model->insertData($data,'tbl_tracing');
				header('location:'.base_url().'index.php/admin/bg_scan_logistik?scan=1/');
				$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															SUKSES !!! 
														</strong>
														<br>Logistik <b>(".$cek_spk['resi_kode'].")</b> tujuan <b>".$cek_spk['resi_tujuan']."</b> berhasil diupdate di ".$trace_lokasi.".</font>
													</p>
												</div>");

				$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'success',
										              title: 'SPK berhasil diupdate!',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");
			}
			else
			{
				header('location:'.base_url().'index.php/admin/bg_scan_logistik?scan=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														SPK sudah discan!<br>Logistik <b>(".$cek_spk['resi_kode'].")</b> tujuan <b>".$cek_spk['resi_tujuan']."</b> berhasil diupdate di ".$trace_lokasi."</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'success',
										              title: 'SPK sudah discan!',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");	
			}
		}
		else
		{
			header('location:'.base_url().'index.php/admin/bg_scan_logistik?scan=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Gagal! - 
														</strong>
														SPK tidak ditemukan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     Swal.fire({
										              position: 'top-end',
										              icon: 'error',
										              title: 'SPK tidak ditemukan!',
										              showConfirmButton: false,
										              timer: 2500
										            })   
											     },10);  
											    </script>
											    ");	
		}
			
	}


	// SCAN LOGISTIK - END

	// MENU PENGIRIMAN - START

	public function bg_pengiriman()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_spk'] 			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'resi_kode','tbl_resi');
			$bc['total_harga'] 			= $this->web_app_model->getSUMAllData('tbl_barang_logistik','barang_hg_total','barang_resi',$this->uri->segment(3));

			$bc['target_penerimaan'] 	= $this->web_app_model->target_penerimaan();
			$bc['penerimaan_saat_ini'] 	= $this->web_app_model->penerimaan_saat_ini();
			$bc['belum_diterima'] 		= $this->web_app_model->belum_diterima();

			$bc['wilayah'] 				= $this->session->userdata('wilayah');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
			$bc['modalTambahLogistik'] 	= $this->load->view('admin/modalTambahLogistik',$bc,true);
			$bc['modalTambahBarang'] 	= $this->load->view('admin/modalTambahBarang',$bc,true);
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/bg_pengiriman',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}

	public function hapus_logistik()
	{
		$resi_kode					= $this->uri->segment(3);
		$resi_file					= $this->uri->segment(4);
		$resi_dok_invoice			= $this->uri->segment(5); 
		$resi_dok_kwitansi			= $this->uri->segment(6); 
		$hapus_tbl_resi 			= array('resi_kode'=>$resi_kode);
		$hapus_tbl_barang_logistik 	= array('barang_resi'=>$resi_kode);
		$hapus_tbl_tracing 			= array('trace_resi'=>$resi_kode);

		$path_qrcode 				= './vendor/qr_code/';
	  	@unlink($path_qrcode.$resi_file);

	  	$path_bast 					= './upload/bast/';
	  	@unlink($path_bast.$resi_file);

	  	$path_invoice 				= './upload/invoice/';
	  	@unlink($path_invoice.$resi_dok_invoice);

	  	$path_kwitansi 				= './upload/kwitansi/';
	  	@unlink($path_kwitansi.$resi_dok_kwitansi);

		$this->web_app_model->deleteData('tbl_resi',$hapus_tbl_resi);
		$this->web_app_model->deleteData('tbl_barang_logistik',$hapus_tbl_barang_logistik);
		$this->web_app_model->deleteData('tbl_tracing',$hapus_tbl_tracing);
		header('location:'.base_url().'index.php/admin/bg_pengiriman?kirim=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Logistik has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Logistik has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}

	public function cetak_surat_jalan()
	{
		$cek  = $this->session->userdata('logged_in');
		$stts = $this->session->userdata('stts');
		if(!empty($cek) && $stts=='Admin Wilayah' || $stts=='Super Admin')
		{

			// CONFIG ============ Wajib Ada
			$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
			// END CONFIG ======== Wajib Ada

			$bc['data_spk'] 			= $this->web_app_model->getWhereOneItem($this->uri->segment(3),'resi_kode','tbl_resi');

			$bc['wilayah'] 				= $this->session->userdata('wilayah');
			$bc['nama'] 				= $this->session->userdata('nama');
			$bc['status'] 				= $this->session->userdata('stts');
		//	$bc['modalTambahLogistik'] 	= $this->load->view('admin/modalTambahLogistik',$bc,true);
		//	$bc['modalTambahBarang'] 	= $this->load->view('admin/modalTambahBarang',$bc,true);
			$bc['atas'] 				= $this->load->view('admin/atas',$bc,true);
			$bc['menu'] 				= $this->load->view('admin/menu',$bc,true);
			$bc['bio'] 					= $this->load->view('admin/bio',$bc,true);	
			$this->load->view('admin/surat/surat_jalan',$bc);
		}
		else
		{
			header('location:'.base_url().'index.php/welcome');
		}
	}



	public function tambahLogistik()
	{
		date_default_timezone_set('Asia/Jakarta');
		$resi_kode			= $this->input->post('resi_kode');
		$resi_tujuan		= $this->input->post('resi_tujuan');
		$resi_internal		= $this->input->post('resi_internal');

		$resi_kendaraan			= $this->input->post('resi_kendaraan');
		$resi_no_polisi			= $this->input->post('resi_no_polisi');
		$resi_lokasi_muat		= $this->input->post('resi_lokasi_muat');
		$resi_alamat_muat1		= $this->input->post('resi_alamat_muat1');
		$resi_alamat_muat2		= $this->input->post('resi_alamat_muat2');
		$resi_lokasi_bongkar	= $this->input->post('resi_lokasi_bongkar');
		$resi_alamat_bongkar1	= $this->input->post('resi_alamat_bongkar1');
		$resi_alamat_bongkar2	= $this->input->post('resi_alamat_bongkar2');

		$resi_created_at	= date('Y-m-d H:i:s');
		$resi_stts			= 'Process';

			

			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
	        $config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = './vendor/qr_code/tmp/'; //string, the default is application/cache/
	        $config['errorlog']     = './vendor/qr_code/tmp/'; //string, the default is application/logs/
	        $config['imagedir']     = './vendor/qr_code/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	 
	        $image_name=$resi_kode.'.png'; //buat name dari qr code sesuai dengan nim
	 
	        $params['data'] = $resi_kode; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
	        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


	        $data = array(		
				'resi_kode' 		=> $resi_kode,
				'resi_tujuan'		=> $resi_tujuan,
				'resi_internal'		=> $resi_internal,

				'resi_kendaraan'		=> $resi_kendaraan,
				'resi_no_polisi'		=> $resi_no_polisi,
				'resi_lokasi_muat'		=> $resi_lokasi_muat,
				'resi_alamat_muat1'		=> $resi_alamat_muat1,
				'resi_alamat_muat2'		=> $resi_alamat_muat2,
				'resi_lokasi_bongkar'	=> $resi_lokasi_bongkar,
				'resi_alamat_bongkar1'	=> $resi_alamat_bongkar1,
				'resi_alamat_bongkar2'	=> $resi_alamat_bongkar2,

				'resi_created_at'	=> $resi_created_at,
				'resi_stts'			=> $resi_stts,
				'resi_qrcode'		=> $image_name,
				);

	        $tracing = array(		
				'trace_resi' 		=> $resi_kode,
				'trace_lokasi'		=> 'Tindak Lanjut',
				'trace_ket'			=> 'Sedang dalam proses tindak lanjut, menunggu muatan logistik',

				'trace_updated_at'		=> $resi_created_at,
				);
			
			$this->web_app_model->insertData($data,'tbl_resi');
			$this->web_app_model->insertData($tracing,'tbl_tracing');

			header('location:'.base_url().'index.php/admin/bg_pengiriman/?kirim=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data logistik berhasil ditambahkan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data logistik berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	public function tambahBarang()
	{
		date_default_timezone_set('Asia/Jakarta');
		$barang_resi		= $this->uri->segment(3);
		$barang_kode		= $this->input->post('barang_kode');
		$barang_nama		= $this->input->post('barang_nama');
		$barang_qty			= $this->input->post('barang_qty');;
		$barang_satuan		= $this->input->post('barang_satuan');;
		$barang_hg_total	= str_replace(".", "", $this->input->post('barang_hg_total'));
		$barang_created_at	= date('Y-m-d H:i:s');

			$data = array(		
				'barang_resi' 		=> $barang_resi,
				'barang_kode' 		=> $barang_kode,
				'barang_nama'		=> $barang_nama,
				'barang_qty'		=> $barang_qty,
				'barang_satuan'		=> $barang_satuan,
				'barang_hg_total'	=> $barang_hg_total,
				'barang_created_at'	=> $barang_created_at,
				);
			
			$this->web_app_model->insertData($data,'tbl_barang_logistik');
			header('location:'.base_url().'index.php/admin/bg_pengiriman/'.$barang_resi.'?kirim=1/');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
													<button type='button' class='close' data-dismiss='alert'>
														<i class='icon-remove'></i>
													</button>
			
													<p><font color='black'>
														<strong>
															<i class='icon-ok'></i>
															Success! - 
														</strong>
														Data barang berhasil ditambahkan!</font>
													</p>
												</div>");

			$this->session->set_flashdata("info2","<script type='text/javascript'>
											     setTimeout(function () { 
											     swal({
											                title: 'Success!!',
											                text:  'Data barang berhasil ditambahkan!',
											                type: 'success',
											                timer: 3000,
											                showConfirmButton: true
											            });  
											     },10);  
											    </script>
											    ");
	}

	function data_pengiriman(){

        $draw 		= $this->input->post('draw');
       	

        if($this->input->post('search')['value'] == '')
        {
        	$search 	= $this->uri->segment(3);
        }
        else
        {
        	$search 	= $this->input->post('search')['value'];
        }

        
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_resi');

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					//case 0:
					//	$this->db->order_by('resi_no', $value['dir']);
					//	break;
					//case 1:
					//	$this->db->order_by('resi_kode', $value['dir']);
					//	break;
					case 0:
						$this->db->order_by('resi_tujuan', $value['dir']);
						break;
					case 1:
						$this->db->order_by('resi_created_at', $value['dir']);
						break;
					case 2:
						$this->db->order_by('resi_stts', $value['dir']);
						break;
					case 3:
						$this->db->order_by('resi_nominal_invoice', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("resi_kode",$search);
			$this->db->or_like("resi_tujuan",$search);
			$this->db->or_like("resi_internal",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "<b>RP</b> " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
	
			$hapus 					= '"Menghapus data logistik ini akan menghapus seluruh data yang berhubungan dengan SPK logistik ini. Are you sure you want to delete the '.$d['resi_kode'].'?"';

			//==========
			if($d['resi_stts'] == "Process")
			{
				$status_spk  = "<span style='text-transform: uppercase;' class='label label-default'>Process</span>";
			}
			else if ($d['resi_stts'] == "Selesai")
			{
				$status_spk  = "<img width='20%' src='".base_url()."vendor/assets/images/web/checked.png'><br>S E L E S A I ";
			}
			//============


			//============
			if($d['resi_dok_invoice'] == "")
			{
				$status_invoice = "<span style='text-transform: uppercase;' class='label label-yellow'><font color='black'>Waiting Invoice</font></span>";
			}
			else if ($d['resi_dok_invoice'] != "" && $d['resi_dok_kwitansi'] == "")
			{
				$status_invoice = "
					<span style='text-transform: uppercase;' class='label label-purple'><font color='white'>Waiting Payment</font></span>
					<!--<a target='_blank' href='".base_url()."upload/invoice/".$d['resi_dok_invoice']."'>
						<img width='24%' src='".base_url()."vendor/assets/images/web/google-docs.png'>
					</a>-->
					";
			}
			else if ($d['resi_dok_invoice'] != "" && $d['resi_dok_kwitansi'] != "")
			{
				$status_invoice = "
					<span style='text-transform: uppercase;' class='label label-green'><font color='white'>F U L L - P A Y M E N T</font></span>
					<!--<a target='_blank' href='".base_url()."upload/invoice/".$d['resi_dok_invoice']."'>
						<img width='24%' src='".base_url()."vendor/assets/images/web/google-docs.png'>
					</a>-->
					";
			}
			//============

			//============
			if($d['resi_dok_invoice'] != "")
			{
				$nominal = rupiah($d['resi_nominal_invoice']);
			}
			else
			{
				$nominal = "Waiting";
			}
			//============

			$output['data'][]=array(
				//"<p align='center'><img width='45px' src='".base_url()."vendor/qr_code/".$d['resi_qrcode']."'</p>",
				//"RESI.".$d['resi_internal']."<br>SPK.<b>".$d['resi_kode']."</b>",
				"<span style='text-transform: uppercase;'><i class='clip-file-2'></i> <b>".$d['resi_kode']."</b><br><i class='clip-truck'></i> ".$d['resi_lokasi_bongkar']."<br><i class='fa fa-map-o'></i> <b>".$d['resi_tujuan']."</b></span>",
				"<p style='text-transform: uppercase;' align='center'><i class='clip-calendar'></i> ".date('d M Y', strtotime($d['resi_created_at']))."<br><i class='clip-clock'></i> ".date('H:i:s', strtotime($d['resi_created_at']))." WIB</p>",
				"<p align='center'>".$status_spk."</p>",
				"<p align='center'>".$nominal."<br>".$status_invoice."</p>",
				"<div class='btn-group'>
                    <a class='btn btn-green dropdown-toggle btn-xs' data-toggle='dropdown' href='#'>
                        <i class='fa fa-cog'></i> <span class='caret'></span>
                    </a>
                    <ul role='menu' class='dropdown-menu pull-right'>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_pengiriman/".$d['resi_kode']."?kirim=1'>
                                <i class='clip-truck'></i> Detail Logistik
                            </a>
                        </li>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/bg_upload_bast/".$d['resi_kode']."?kirim=1'>
                                <i class='clip-file-2'></i> Unggah Dokumen
                            </a>
                        </li>
                        <li role='presentation'>
                            <a role='menuitem' tabindex='-1' onclick='return confirm(".$hapus.")' href='".base_url()."index.php/admin/hapus_logistik/".$d['resi_kode']."/".$d['resi_qrcode']."/".$d['resi_dok_invoice']."/".$d['resi_dok_kwitansi']."?kirim=1'>
                                <i class='fa fa-trash-o'></i> Hapus Logistik
                            </a>
                        </li>
                    </ul>
                </div>");
			$no++;
		}

		$output['recordsTotal'] = $this->db->from('tbl_resi')
											->get()->num_rows();

		echo json_encode($output);
    } 

    function data_tracking(){

    	$spk 		= $this->uri->segment(3);
        $draw 		= $this->input->post('draw');
        $search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_tracing')
				 ->where('trace_resi',$spk);

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('trace_no', $value['dir']);
						break;
					case 1:
						$this->db->order_by('trace_lokasi', $value['dir']);
						break;
					case 2:
						$this->db->order_by('trace_ket', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("trace_resi",$search);
			$this->db->or_like("trace_lokasi",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
	
			$hapus 					= '"Are you sure you want to delete the '.$d['trace_resi'].'?"';

			$output['data'][]=array(
				"<p align='center'>".date('d/m/Y', strtotime($d['trace_updated_at']))."<br>".date('H:i', strtotime($d['trace_updated_at']))." WIB</p>",
				$d['trace_lokasi'],
				$d['trace_ket']);
			$no++;
		}

		$output['recordsTotal'] = $this->db->from('tbl_tracing')
											->where('trace_resi',$spk)
											->get()->num_rows();

		echo json_encode($output);
    } 

    function data_barang(){

    	$spk 		= $this->uri->segment(3);
        $draw 		= $this->input->post('draw');
       	$search 	= $this->input->post('search')['value'];
        $start 		= $this->input->post('start');
        $length 	= $this->input->post('length');
        $order 		= $this->input->post('order');

		$this->db->from('tbl_barang_logistik')
					->where('barang_resi',$spk);

		if($order){
			foreach ($order as $value) {
				switch ($value['column']) {
					case 0:
						$this->db->order_by('barang_no', $value['dir']);
						break;
					case 1:
						$this->db->order_by('barang_nama', $value['dir']);
						break;
					case 2:
						$this->db->order_by('barang_qty', $value['dir']);
						break;
					case 3:
						$this->db->order_by('barang_satuan', $value['dir']);
						break;
					case 4:
						$this->db->order_by('barang_hg_total', $value['dir']);
						break;
				}
			}
		}
			
		if($search!=""){
			$this->db->like("barang_nama",$search);
			//$this->db->or_like("resi_tujuan",$search);
		}

		$tempdb = clone $this->db;
		$output['recordsFiltered'] = $tempdb->count_all_results();

		$this->db->limit($length,$start);

		$data = $this->db->get()->result_array();

		function rupiah($angka){
		    $hasil_rupiah = "Rp" . number_format($angka,0,',','.');
		    return $hasil_rupiah;
    	}

    	$no=1;
		foreach ($data as $d) {
	
			$hapus 					= '"Are you sure you want to delete the '.$d['barang_nama'].'?"';

			$output['data'][]=array(
				"<p align='center'>".$no."</p>",
				"<span style='text-transform:;'>".$d['barang_nama']."</span>",
				"<p align='center'><span style='text-transform:;'>".$d['barang_qty']." ".$d['barang_satuan']."</span></p>",
				"<p align='center'><span style='text-transform:;'>".rupiah($d['barang_hg_total'])."</span></p>",
				"<a role='menuitem' tabindex='-1' href='".base_url()."index.php/admin/hapus_barang/".$d['barang_no']."/".$this->uri->segment(3)."' onclick='return confirm(".$hapus.")'>
                    <font color='red'><img width='100%' src='".base_url()."vendor/assets/images/web/delete.png'></font>
                 </a>");
			$no++;
		}

		$output['recordsTotal'] = $this->db->from('tbl_barang_logistik')
											->where('barang_resi',$spk)
											->get()->num_rows();

		echo json_encode($output);
    } 

    public function hapus_barang()
	{
		$barang_no		= $this->uri->segment(3);
		$spk			= $this->uri->segment(4);
		$hapus 			= array('barang_no'=>$barang_no);

		$this->web_app_model->deleteData('tbl_barang_logistik',$hapus);
		header('location:'.base_url().'index.php/admin/bg_pengiriman/'.$spk.'?kirim=1');
		$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
									<button type='button' class='close' data-dismiss='alert'>
										<i class='icon-remove'></i>
									</button>

									<p>
										<strong>
											<i class='icon-ok'></i>
											Success! - 
										</strong>
										Item has been deleted!
									</p>
								</div>");

		$this->session->set_flashdata("info2","<script type='text/javascript'>
										     setTimeout(function () { 
										     swal({
										                title: 'Success!!',
										                text:  'Item has been deleted!',
										                type: 'success',
										                timer: 3000,
										                showConfirmButton: true
										            });  
										     },10);  
										    </script>
										    ");
	}


	// MENU PENGIRIMAN - END

}
