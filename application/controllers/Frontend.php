<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {

	/**
	 * Created by Ilham Ramadhan, S.Tr.Kom
	 * IT PDAM Kota langsa
	 * HP : 0853 6188 5100
	 * Email: ilhamr6000@gmail.com
	 */
	

	public function index()
	{
		// CONFIG ============ Wajib Ada
		$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
		// END CONFIG ======== Wajib Ada

		$bc['nama'] 			= $this->session->userdata('nama');
		$bc['status'] 			= $this->session->userdata('stts');
		//$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
		//$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
		//$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
		$this->load->view('frontend/bg_home',$bc);
	}

	public function bg_lacak()
	{
		// CONFIG ============ Wajib Ada
		$bc['data_config']		= $this->web_app_model->getWhereOneItem('1','id_config','tbl_config');
		// END CONFIG ======== Wajib Ada

		$trace_resi 			= $this->uri->segment(3);
		$bc['data_spk'] 		= $this->web_app_model->getWhereAllItem($trace_resi,'trace_resi','tbl_tracing');

		$bc['nama'] 			= $this->session->userdata('nama');
		$bc['status'] 			= $this->session->userdata('stts');
		//$bc['atas'] 			= $this->load->view('admin/atas',$bc,true);
		//$bc['menu'] 			= $this->load->view('admin/menu',$bc,true);
		//$bc['bio'] 				= $this->load->view('admin/bio',$bc,true);	
		$this->load->view('frontend/bg_lacak',$bc);
	}

	public function lacak_logistik()
	{

		$trace_resi				= $this->input->post('trace_resi');
		$cek_spk  				= $this->web_app_model->getWhereOneItem($trace_resi,'trace_resi','tbl_tracing');

		if(empty($cek_spk['trace_resi']))
		{	
			header('location:'.base_url().'index.php/frontend/bg_lacak/'.$trace_resi.'#');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-danger'>
														<b>Mohon maaf !!</b> - Pengiriman Logistik dengan SPK $trace_resi tidak ditemukan! atau belum diupdate oleh petugas. Cek kembali secara berkala
												</div>
												<div class='col-md-12'> 
							                        <p align='center'><font size='4'>Maaf, data <b>$trace_resi</b> tidak tersedia!<br>Atau mungkin saja belum diupdate oleh administrator. Silahkan <b>Cek kembali</b> secara berkala dan pastikan input No SPK dengan benar. Terimakasih :)</font><br><br><br>
							                            <img width='13%'' src='".base_url()."vendor/assets/images/web/sad.png'>
							                        </p>
							                        <br>
							                    </div>");
		}
		else
		{	
			header('location:'.base_url().'index.php/frontend/bg_lacak/'.$trace_resi.'#');
			$this->session->set_flashdata("info","<div class='alert alert-block alert-success'>
														<b>Suksess !! - </b>Riwayat pengiriman Logistik ditemukan!
													
												</div>");
		}
	}
}
